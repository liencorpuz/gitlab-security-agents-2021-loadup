#!/bin/bash

echo "===================================================="
echo "GLOBE DSG INFRASTRUCTURE"
echo "WELCOME! DSG INSTALLATION OF SEC-AGENTS"
echo "===================================================="
echo

#INSTANCE_IDINFO=$(cat ec2instanceid.txt)

if [ -f /etc/os-release ]; then
    . /etc/os-release
    OS=$NAME
    VER=$VERSION_ID
    ID_OS=$ID
    OS_VER="${VER:0:2}"
    OS_VER_AMZ="${VER:0:4}"
    echo $NAME "$OS_VER"
    UNAME="admin"
    PWORD="9<70j>SS"
    DNSNAME="vpce-083fd8e573c1723dc-uhy62nzl.vpce-svc-04193c7509b9ae7fe.ap-southeast-1.vpce.amazonaws.com:9997"

    if [ $ID == "centos" ] && [ $OS_VER -eq "7" ]
    then
        cd /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers/
        #Install Crowdstrike
        sudo yum install -y libnl-genl-3-200
        sudo yum install -y libnl
        sudo rpm -i falcon-sensor-6.14.0-11110.amzn2.x86_64.rpm
        sudo /opt/CrowdStrike/falconctl -s --cid="8C81692122F14CE8BA112211F6B3A09A-41"
        sudo /opt/CrowdStrike/falconctl -s --apd=TRUE
        sudo service falcon-sensor start
        #######################################################################################
        #Install Nessus
        sudo rpm -i NessusAgent-7.7.0-amzn.x86_64.rpm
        sudo  /opt/nessus_agent/sbin/nessuscli agent link --key=e95d30247248117044465c5bbc9ad271ef4723234c591248521534fddd17ecb8 --cloud="yes" --groups="All Nessus Agents,Digital Ventures"
        sudo service nessusagent start
        cd /opt/nessus_agent/sbin/
        sudo ./nessuscli agent status
        ########################################################################################
        #Move and extract Splunk
        sudo tar -xvzf /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers/splunkforwarder-8.0.6-152fb4b2bb96-Linux-x86_64.tgz -C /opt/
        cd /opt/splunkforwarder/bin
        sudo ./splunk start --accept-license --answer-yes --no-prompt
        sudo ./splunk enable boot-start 
        cd /opt/splunkforwarder/etc/system/local
        sudo cp /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers/SplunkInstaller-ISDP/deploymentclient.conf /opt/splunkforwarder/etc/system/local/
        sudo sed -i '7 i allowSslCompression = false' server.conf
        sudo sed -i '8 i sslVersions = tls' server.conf
        echo -e "...\n\n Completed Configuring deployment.conf and server.conf.....Restarting the Splunk Agent"
        cd /opt/splunkforwarder/bin/
        sudo ./splunk restart
        sudo ./splunk status
        #2nd step of Splunk
        echo "Configuring the 2nd Step for Splunk"
        sudo sleep 3
        sudo cp -r /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-isdp-linux-config /opt/splunkforwarder/etc/apps/
        sudo cp -r /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-vpcendpoint-outputs /opt/splunkforwarder/etc/apps/
        cd /opt/splunkforwarder/etc/apps/TA-isdp-linux-config/bin/
        sudo chmod +x *.sh
        sudo cp /tmp/secagents2021/gitlab-security-agents-2021-loadup/user-seed.conf /opt/splunkforwarder/etc/system/local/
        echo "Completed the 2nd Step for Splunk.....Restarting the Splunk Agent"
        cd /opt/splunkforwarder/bin/
        ./splunk restart
        sudo service splunk start
        ./splunk status
    
    fi

    if [ $ID == "ubuntu" ] && [ $OS_VER == "16" ]
    then
        cd /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers
        #Install Crowdstrike
        sudo apt-get install -y libnl-genl-3-200
        sudo dpkg -i falcon-sensor_6.14.0-11110_amd64.deb
        sudo /opt/CrowdStrike/falconctl -s --cid="8C81692122F14CE8BA112211F6B3A09A-41"
        sudo /opt/CrowdStrike/falconctl -s --apd=TRUE
        sudo service falcon-sensor start
        #######################################################################################
        #Install Nessus
        sudo dpkg -i NessusAgent-7.7.0-ubuntu1110_amd64.deb
        sudo  /opt/nessus_agent/sbin/nessuscli agent link --key=e95d30247248117044465c5bbc9ad271ef4723234c591248521534fddd17ecb8 --cloud="yes" --groups="All Nessus Agents,Digital Ventures"
        sudo service nessusagent start
        cd /opt/nessus_agent/sbin/
        sudo ./nessuscli agent status
		#######################################################################################
        #Move and extract Splunk
        sudo tar -xvzf /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers/splunkforwarder-8.0.6-152fb4b2bb96-Linux-x86_64.tgz -C /opt/
        cd /opt/splunkforwarder/bin
        sudo ./splunk start --accept-license --answer-yes --no-prompt
        sudo ./splunk enable boot-start 
        cd /opt/splunkforwarder/etc/system/local
        sudo cp /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers/SplunkInstaller-ISDP/deploymentclient.conf /opt/splunkforwarder/etc/system/local/
        sudo sed -i '7 i allowSslCompression = false' server.conf
        sudo sed -i '8 i sslVersions = tls' server.conf
        echo -e "...\n\n Completed Configuring deployment.conf and server.conf.....Restarting the Splunk Agent"
        cd /opt/splunkforwarder/bin/
        sudo ./splunk restart
        sudo ./splunk status
        #2nd step of Splunk
        echo "Configuring the 2nd Step for Splunk"
        sudo sleep 3
        sudo cp -r /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-isdp-linux-config /opt/splunkforwarder/etc/apps/
        sudo cp -r /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-vpcendpoint-outputs /opt/splunkforwarder/etc/apps/
        cd /opt/splunkforwarder/etc/apps/TA-isdp-linux-config/bin/
        sudo chmod +x *.sh
        sudo cp /tmp/secagents2021/gitlab-security-agents-2021-loadup/user-seed.conf /opt/splunkforwarder/etc/system/local/
        echo "Completed Configuring the 2nd Step for Splunk.....Restarting the Splunk Agent"
        cd /opt/splunkforwarder/bin/
        ./splunk restart
        sudo service splunk start
        ./splunk status
    fi

       if [ $ID == "ubuntu" ] && [ $OS_VER == "18" ]
    then
        cd /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers
        #Install Crowdstrike
        sudo apt-get install -y libnl-genl-3-200
        sudo dpkg -i falcon-sensor_6.14.0-11110_amd64.deb
        sudo /opt/CrowdStrike/falconctl -s --cid="8C81692122F14CE8BA112211F6B3A09A-41"
        sudo /opt/CrowdStrike/falconctl -s --apd=TRUE
        sudo service falcon-sensor start
        #######################################################################################
        #Install Nessus
        sudo dpkg -i NessusAgent-7.7.0-ubuntu1110_amd64.deb 
        sudo  /opt/nessus_agent/sbin/nessuscli agent link --key=e95d30247248117044465c5bbc9ad271ef4723234c591248521534fddd17ecb8 --cloud="yes" --groups="All Nessus Agents,Digital Ventures"
        sudo service nessusagent start
        cd /opt/nessus_agent/sbin/
        sudo ./nessuscli agent status
		#######################################################################################
        #Move and extract Splunk
        sudo tar -xvzf /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers/splunkforwarder-8.0.6-152fb4b2bb96-Linux-x86_64.tgz -C /opt/
        cd /opt/splunkforwarder/bin
        sudo ./splunk start --accept-license --answer-yes --no-prompt
        sudo ./splunk enable boot-start 
        cd /opt/splunkforwarder/etc/system/local
        sudo cp /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers/SplunkInstaller-ISDP/deploymentclient.conf /opt/splunkforwarder/etc/system/local/
        sudo sed -i '7 i allowSslCompression = false' server.conf
        sudo sed -i '8 i sslVersions = tls' server.conf
        echo -e "...\n\n Completed Configuring deployment.conf and server.conf.....Restarting the Splunk Agent"
        cd /opt/splunkforwarder/bin/
        sudo ./splunk restart
        sudo ./splunk status
        #2nd step of Splunk
        echo "Configuring the 2nd Step for Splunk"
        sudo sleep 3
        sudo cp -r /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-isdp-linux-config /opt/splunkforwarder/etc/apps/
        sudo cp -r /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-vpcendpoint-outputs /opt/splunkforwarder/etc/apps/
        cd /opt/splunkforwarder/etc/apps/TA-isdp-linux-config/bin/
        sudo chmod +x *.sh
        sudo cp /tmp/secagents2021/gitlab-security-agents-2021-loadup/user-seed.conf /opt/splunkforwarder/etc/system/local/
        echo "Completed Configuring the 2nd Step for Splunk.....Restarting the Splunk Agent"
        cd /opt/splunkforwarder/bin/
        ./splunk restart
        sudo service splunk start
        ./splunk status

    fi

           if [ $ID == "ubuntu" ] && [ $OS_VER == "20" ]
    then
        cd /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers
        #Install Crowdstrike
        sudo apt-get install -y libnl-genl-3-200
        sudo dpkg -i falcon-sensor_6.14.0-11110_amd64.deb
        sudo /opt/CrowdStrike/falconctl -s --cid="8C81692122F14CE8BA112211F6B3A09A-41"
        sudo /opt/CrowdStrike/falconctl -s --apd=TRUE
        sudo service falcon-sensor start
        #######################################################################################
        #Install Nessus
        sudo dpkg -i NessusAgent-7.7.0-ubuntu1110_amd64.deb 
        sudo  /opt/nessus_agent/sbin/nessuscli agent link --key=e95d30247248117044465c5bbc9ad271ef4723234c591248521534fddd17ecb8 --cloud="yes" --groups="All Nessus Agents,Digital Ventures"
        sudo service nessusagent start
		cd /opt/nessus_agent/sbin/
        sudo ./nessuscli agent status
		#######################################################################################
        #Move and extract Splunk
        sudo tar -xvzf /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers/splunkforwarder-8.0.6-152fb4b2bb96-Linux-x86_64.tgz -C /opt/
        cd /opt/splunkforwarder/bin
        sudo ./splunk start --accept-license --answer-yes --no-prompt
        sudo ./splunk enable boot-start 
        cd /opt/splunkforwarder/etc/system/local
        sudo cp /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers/SplunkInstaller-ISDP/deploymentclient.conf /opt/splunkforwarder/etc/system/local/
        sudo sed -i '7 i allowSslCompression = false' server.conf
        sudo sed -i '8 i sslVersions = tls' server.conf
        echo -e "...\n\n Completed Configuring deployment.conf and server.conf.....Restarting the Splunk Agent"
        cd /opt/splunkforwarder/bin/
        sudo ./splunk restart
        sudo ./splunk status
        #2nd step of Splunk
        echo "Configuring the 2nd Step for Splunk"
        sudo sleep 3
        sudo cp -r /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-isdp-linux-config /opt/splunkforwarder/etc/apps/
        sudo cp -r /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-vpcendpoint-outputs /opt/splunkforwarder/etc/apps/
        cd /opt/splunkforwarder/etc/apps/TA-isdp-linux-config/bin/
        sudo chmod +x *.sh
        sudo cp /tmp/secagents2021/gitlab-security-agents-2021-loadup/user-seed.conf /opt/splunkforwarder/etc/system/local/
        echo "Completed Configuring the 2nd Step for Splunk.....Restarting the Splunk Agent"
        cd /opt/splunkforwarder/bin/
        ./splunk restart
        sudo service splunk start
        ./splunk status
    fi


    if [ $ID == "ubuntu" ] && [ $OS_VER == "14" ]
    then
        cd /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers
        #Install Crowdstrike
        sudo apt-get install -y libnl-genl-3-200
        sudo dpkg -i falcon-sensor_6.14.0-11110_amd64.deb
        sudo /opt/CrowdStrike/falconctl -s --cid="8C81692122F14CE8BA112211F6B3A09A-41"
        sudo /opt/CrowdStrike/falconctl -s --apd=TRUE
        sudo service falcon-sensor start
        #######################################################################################
        #Install Nessus
        sudo dpkg -i NessusAgent-7.7.0-ubuntu1110_amd64.deb 
        sudo  /opt/nessus_agent/sbin/nessuscli agent link --key=e95d30247248117044465c5bbc9ad271ef4723234c591248521534fddd17ecb8 --cloud="yes" --groups="All Nessus Agents,Digital Ventures"
        sudo service nessusagent start
		cd /opt/nessus_agent/sbin/
        sudo ./nessuscli agent status
		#######################################################################################
        #Move and extract Splunk
        sudo tar -xvzf /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers/splunkforwarder-8.0.6-152fb4b2bb96-Linux-x86_64.tgz -C /opt/
        cd /opt/splunkforwarder/bin
        sudo ./splunk start --accept-license --answer-yes --no-prompt
        sudo ./splunk enable boot-start 
        cd /opt/splunkforwarder/etc/system/local
        sudo cp /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers/SplunkInstaller-ISDP/deploymentclient.conf /opt/splunkforwarder/etc/system/local/
        sudo sed -i '7 i allowSslCompression = false' server.conf
        sudo sed -i '8 i sslVersions = tls' server.conf
        echo -e "...\n\n Completed Configuring deployment.conf and server.conf.....Restarting the Splunk Agent"
        cd /opt/splunkforwarder/bin/
        sudo ./splunk restart
        sudo ./splunk status
        #2nd step of Splunk
        echo "Configuring the 2nd Step for Splunk"
        sudo sleep 3
        sudo cp -r /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-isdp-linux-config /opt/splunkforwarder/etc/apps/
        sudo cp -r /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-vpcendpoint-outputs /opt/splunkforwarder/etc/apps/
        cd /opt/splunkforwarder/etc/apps/TA-isdp-linux-config/bin/
        sudo chmod +x *.sh
        sudo cp /tmp/secagents2021/gitlab-security-agents-2021-loadup/user-seed.conf /opt/splunkforwarder/etc/system/local/
        echo "Completed Configuring the 2nd Step for Splunk.....Restarting the Splunk Agent"
        cd /opt/splunkforwarder/bin/
        ./splunk restart
        sudo service splunk start
        ./splunk status

    fi


    if [ $ID == "amzn" ] && [ $OS_VER_AMZ == "2018" ]
    then
        cd /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers
        #Install Crowdstrike
        sudo yum install -y libnl-genl-3-200
        sudo yum install -y libnl
        sudo rpm -i falcon-sensor-6.14.0-11110.amzn2.x86_64.rpm
        sudo /opt/CrowdStrike/falconctl -s --cid="8C81692122F14CE8BA112211F6B3A09A-41"
        sudo /opt/CrowdStrike/falconctl -s --apd=TRUE
        sudo service falcon-sensor start
        #######################################################################################
        #Install Nessus
        sudo rpm -i NessusAgent-7.7.0-amzn.x86_64.rpm
        sudo  /opt/nessus_agent/sbin/nessuscli agent link --key=e95d30247248117044465c5bbc9ad271ef4723234c591248521534fddd17ecb8 --cloud="yes" --groups="All Nessus Agents,Digital Ventures"
        sudo service nessusagent start
		cd /opt/nessus_agent/sbin/
        sudo ./nessuscli agent status
		#######################################################################################
        #Move and extract Splunk
        sudo tar -xvzf /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers/splunkforwarder-8.0.6-152fb4b2bb96-Linux-x86_64.tgz -C /opt/
        cd /opt/splunkforwarder/bin
        sudo ./splunk start --accept-license --answer-yes --no-prompt
        sudo ./splunk enable boot-start 
        cd /opt/splunkforwarder/etc/system/local
        sudo cp /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers/SplunkInstaller-ISDP/deploymentclient.conf /opt/splunkforwarder/etc/system/local/
        sudo sed -i '7 i allowSslCompression = false' server.conf
        sudo sed -i '8 i sslVersions = tls' server.conf
        echo -e "...\n\n Completed Configuring deployment.conf and server.conf.....Restarting the Splunk Agent"
        cd /opt/splunkforwarder/bin/
        sudo ./splunk restart
        sudo ./splunk status
        #2nd step of Splunk
        echo "Configuring the 2nd Step for Splunk"
        sudo sleep 3
        sudo cp -r /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-isdp-linux-config /opt/splunkforwarder/etc/apps/
        sudo cp -r /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-vpcendpoint-outputs /opt/splunkforwarder/etc/apps/
        cd /opt/splunkforwarder/etc/apps/TA-isdp-linux-config/bin/
        sudo chmod +x *.sh
        sudo cp /tmp/secagents2021/gitlab-security-agents-2021-loadup/user-seed.conf /opt/splunkforwarder/etc/system/local/
        echo "Completed Configuring the 2nd Step for Splunk.....Restarting the Splunk Agent"
        cd /opt/splunkforwarder/bin/
        ./splunk restart
        sudo service splunk start
        ./splunk status
    fi

    if [ $ID == "amzn" ] && [ $OS_VER_AMZ == "2015" ]
    then
        cd /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers
        #Install Crowdstrike
        sudo yum install -y libnl-genl-3-200
        sudo yum install -y libnl
        sudo rpm -i falcon-sensor-6.14.0-11110.amzn2.x86_64.rpm
        sudo /opt/CrowdStrike/falconctl -s --cid="8C81692122F14CE8BA112211F6B3A09A-41"
        sudo /opt/CrowdStrike/falconctl -s --apd=TRUE
        sudo service falcon-sensor start
        #######################################################################################
        #Install Nessus
        sudo rpm -i NessusAgent-7.7.0-amzn.x86_64.rpm
        sudo  /opt/nessus_agent/sbin/nessuscli agent link --key=e95d30247248117044465c5bbc9ad271ef4723234c591248521534fddd17ecb8 --cloud="yes" --groups="All Nessus Agents,Digital Ventures"
        sudo service nessusagent start
		cd /opt/nessus_agent/sbin/
        sudo ./nessuscli agent status
		#######################################################################################
        #Move and extract Splunk
        sudo tar -xvzf /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers/splunkforwarder-8.0.6-152fb4b2bb96-Linux-x86_64.tgz -C /opt/
        cd /opt/splunkforwarder/bin
        sudo ./splunk start --accept-license --answer-yes --no-prompt
        sudo ./splunk enable boot-start 
        cd /opt/splunkforwarder/etc/system/local
        sudo cp /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers/SplunkInstaller-ISDP/deploymentclient.conf /opt/splunkforwarder/etc/system/local/
        sudo sed -i '7 i allowSslCompression = false' server.conf
        sudo sed -i '8 i sslVersions = tls' server.conf
        echo -e "...\n\n Completed Configuring deployment.conf and server.conf.....Restarting the Splunk Agent"
        cd /opt/splunkforwarder/bin/
        sudo ./splunk restart
        sudo ./splunk status
        #2nd step of Splunk
        echo "Configuring the 2nd Step for Splunk"
        sudo sleep 3
        sudo cp -r /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-isdp-linux-config /opt/splunkforwarder/etc/apps/
        sudo cp -r /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-vpcendpoint-outputs /opt/splunkforwarder/etc/apps/
        cd /opt/splunkforwarder/etc/apps/TA-isdp-linux-config/bin/
        sudo chmod +x *.sh
        sudo cp /tmp/secagents2021/gitlab-security-agents-2021-loadup/user-seed.conf /opt/splunkforwarder/etc/system/local/
        echo "Completed Configuring the 2nd Step for Splunk.....Restarting the Splunk Agent"
        cd /opt/splunkforwarder/bin/
        ./splunk restart
        sudo service splunk start
        ./splunk status

    fi

    if [ $ID == "amzn" ] && [ $OS_VER_AMZ == "2017" ]
    then
        cd /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers
        #Install Crowdstrike
        sudo yum install -y libnl-genl-3-200
        sudo yum install -y libnl
        sudo rpm -i falcon-sensor-6.14.0-11110.amzn2.x86_64.rpm
        sudo /opt/CrowdStrike/falconctl -s --cid="8C81692122F14CE8BA112211F6B3A09A-41"
        sudo /opt/CrowdStrike/falconctl -s --apd=TRUE
        sudo service falcon-sensor start
        #######################################################################################
        #Install Nessus
        sudo rpm -i NessusAgent-7.7.0-amzn.x86_64.rpm
        sudo  /opt/nessus_agent/sbin/nessuscli agent link --key=e95d30247248117044465c5bbc9ad271ef4723234c591248521534fddd17ecb8 --cloud="yes" --groups="All Nessus Agents,Digital Ventures"
        sudo service nessusagent start
		cd /opt/nessus_agent/sbin/
        sudo ./nessuscli agent status
		#######################################################################################
        #Move and extract Splunk
        sudo tar -xvzf /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers/splunkforwarder-8.0.6-152fb4b2bb96-Linux-x86_64.tgz -C /opt/
        cd /opt/splunkforwarder/bin
        sudo ./splunk start --accept-license --answer-yes --no-prompt
        sudo ./splunk enable boot-start 
        cd /opt/splunkforwarder/etc/system/local
        sudo cp /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers/SplunkInstaller-ISDP/deploymentclient.conf /opt/splunkforwarder/etc/system/local/
        sudo sed -i '7 i allowSslCompression = false' server.conf
        sudo sed -i '8 i sslVersions = tls' server.conf
        echo -e "...\n\n Completed Configuring deployment.conf and server.conf.....Restarting the Splunk Agent"
        cd /opt/splunkforwarder/bin/
        sudo ./splunk restart
        sudo ./splunk status
        #2nd step of Splunk
        echo "Configuring the 2nd Step for Splunk"
        sudo sleep 3
        sudo cp -r /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-isdp-linux-config /opt/splunkforwarder/etc/apps/
        sudo cp -r /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-vpcendpoint-outputs /opt/splunkforwarder/etc/apps/
        cd /opt/splunkforwarder/etc/apps/TA-isdp-linux-config/bin/
        sudo chmod +x *.sh
        sudo cp /tmp/secagents2021/gitlab-security-agents-2021-loadup/user-seed.conf /opt/splunkforwarder/etc/system/local/
        echo "Completed Configuring the 2nd Step for Splunk.....Restarting the Splunk Agent"
        cd /opt/splunkforwarder/bin/
        ./splunk restart
        sudo service splunk start
        ./splunk status

    fi

    if [ $ID == "amzn" ] && [ $OS_VER == "2" ]
    then
        cd /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers
        #Install Crowdstrike
        sudo yum install -y libnl-genl-3-200
        sudo yum install -y libnl
        sudo rpm -i falcon-sensor-6.14.0-11110.amzn2.x86_64.rpm
        sudo /opt/CrowdStrike/falconctl -s --cid="8C81692122F14CE8BA112211F6B3A09A-41"
        sudo /opt/CrowdStrike/falconctl -s --apd=TRUE
        sudo service falcon-sensor start
        #######################################################################################
        #Install Nessus
        sudo rpm -i NessusAgent-7.7.0-amzn.x86_64.rpm
        sudo  /opt/nessus_agent/sbin/nessuscli agent link --key=e95d30247248117044465c5bbc9ad271ef4723234c591248521534fddd17ecb8 --cloud="yes" --groups="All Nessus Agents,Digital Ventures"
        sudo service nessusagent start
		cd /opt/nessus_agent/sbin/
        sudo ./nessuscli agent status
		#######################################################################################
        #Move and extract Splunk
        sudo tar -xvzf /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers/splunkforwarder-8.0.6-152fb4b2bb96-Linux-x86_64.tgz -C /opt/
        cd /opt/splunkforwarder/bin
        sudo ./splunk start --accept-license --answer-yes --no-prompt
        /opt/splunkforwarder/bin/splunk start
        sudo ./splunk enable boot-start 
        cd /opt/splunkforwarder/etc/system/local
        sudo cp /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers/SplunkInstaller-ISDP/deploymentclient.conf /opt/splunkforwarder/etc/system/local/
        sudo sed -i '7 i allowSslCompression = false' server.conf
        sudo sed -i '8 i sslVersions = tls' server.conf
        echo -e "...\n\n Completed Configuring deployment.conf and server.conf.....Restarting the Splunk Agent"
        cd /opt/splunkforwarder/bin/
        sudo ./splunk restart
        sudo ./splunk status
        #2nd step of Splunk
        echo "Configuring the 2nd Step for Splunk"
        sudo sleep 3
        sudo cp -r /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-isdp-linux-config /opt/splunkforwarder/etc/apps/
        sudo cp -r /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-vpcendpoint-outputs /opt/splunkforwarder/etc/apps/
        cd /opt/splunkforwarder/etc/apps/TA-isdp-linux-config/bin/
        sudo chmod +x *.sh
        sudo cp /tmp/secagents2021/gitlab-security-agents-2021-loadup/user-seed.conf /opt/splunkforwarder/etc/system/local/
        echo "Completed Configuring the 2nd Step for Splunk.....Restarting the Splunk Agent"
        cd /opt/splunkforwarder/bin/
        ./splunk restart
        sudo service splunk start
        ./splunk status


    fi




    if [ $ID == "rhel" ] && [ $OS_VER == "7" ]
    then
        cd /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers
        #Install Crowdstrike
        sudo yum install -y libnl-genl-3-200
        sudo yum install -y libnl
        sudo rpm -i falcon-sensor-6.14.0-11110.amzn2.x86_64.rpm
        sudo /opt/CrowdStrike/falconctl -s --cid="8C81692122F14CE8BA112211F6B3A09A-41"
        sudo /opt/CrowdStrike/falconctl -s --apd=TRUE
        sudo service falcon-sensor start
        #######################################################################################
        #Install Nessus
        sudo rpm -i NessusAgent-7.7.0-amzn.x86_64.rpm
        sudo  /opt/nessus_agent/sbin/nessuscli agent link --key=e95d30247248117044465c5bbc9ad271ef4723234c591248521534fddd17ecb8 --cloud="yes" --groups="All Nessus Agents,Digital Ventures"
        sudo service nessusagent start
		cd /opt/nessus_agent/sbin/
        sudo ./nessuscli agent status
		#######################################################################################
        #Move and extract Splunk
        sudo tar -xvzf /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers/splunkforwarder-8.0.6-152fb4b2bb96-Linux-x86_64.tgz -C /opt/
        cd /opt/splunkforwarder/bin
        sudo ./splunk start --accept-license --answer-yes --no-prompt
        sudo ./splunk enable boot-start 
        cd /opt/splunkforwarder/etc/system/local
        sudo cp /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers/SplunkInstaller-ISDP/deploymentclient.conf /opt/splunkforwarder/etc/system/local/
        sudo sed -i '7 i allowSslCompression = false' server.conf
        sudo sed -i '8 i sslVersions = tls' server.conf
        echo -e "...\n\n Completed Configuring deployment.conf and server.conf.....Restarting the Splunk Agent"
        cd /opt/splunkforwarder/bin/
        sudo ./splunk restart
        sudo ./splunk status
        #2nd step of Splunk
        echo "Configuring the 2nd Step for Splunk"
        sudo sleep 3
        sudo cp -r /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-isdp-linux-config /opt/splunkforwarder/etc/apps/
        sudo cp -r /tmp/secagents2021/gitlab-security-agents-2021-loadup/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-vpcendpoint-outputs /opt/splunkforwarder/etc/apps/
        cd /opt/splunkforwarder/etc/apps/TA-isdp-linux-config/bin/
        sudo chmod +x *.sh
        sudo cp /tmp/secagents2021/gitlab-security-agents-2021-loadup/user-seed.conf /opt/splunkforwarder/etc/system/local/
        echo "Completed Configuring the 2nd Step for Splunk.....Restarting the Splunk Agent"
        cd /opt/splunkforwarder/bin/
        ./splunk restart
        sudo service splunk start
        ./splunk status

    fi

fi

    echo
    echo "Completed..."

 

#DS AGENT
ACTIVATIONURL='dsm://agents.deepsecurity.trendmicro.com:443/'
MANAGERURL='https://app.deepsecurity.trendmicro.com:443'
CURLOPTIONS='--silent --tlsv1.2'
linuxPlatform='';
isRPM='';

    if [[ $(/usr/bin/id -u) -ne 0 ]]; then
        echo You are not running as the root user.  Please try again with root privileges.;
        logger -t You are not running as the root user.  Please try again with root privileges.;
        exit 1;
    fi;

    if ! type curl >/dev/null 2>&1; then
        echo "Please install CURL before running this script."
        logger -t Please install CURL before running this script
        exit 1
    fi

    CURLOUT=$(eval curl $MANAGERURL/software/deploymentscript/platform/linuxdetectscriptv1/ -o /tmp/PlatformDetection $CURLOPTIONS;)
    err=$?

    if [[ $err -eq 60 ]]; then
        echo "TLS certificate validation for the agent package download has failed. Please check that your Deep Security Manager TLS certificate is signed by a trusted root certificate authority. For more information, search for \"deployment scripts\" in the Deep Security Help Center."
        logger -t TLS certificate validation for the agent package download has failed. Please check that your Deep Security Manager TLS certificate is signed by a trusted root certificate authority. For more information, search for \"deployment scripts\" in the Deep Security Help Center.
        exit 1;
    fi

    if [ -s /tmp/PlatformDetection ]; then
        . /tmp/PlatformDetection
    else
        echo "Failed to download the agent installation support script."
        logger -t Failed to download the Deep Security Agent installation support script
        exit 1
    fi

    platform_detect
    if [[ -z "${linuxPlatform}" ]] || [[ -z "${isRPM}" ]]; then
        echo Unsupported platform is detected
        logger -t Unsupported platform is detected
        exit 1
    fi

    echo Downloading agent package...
    if [[ $isRPM == 1 ]]; then package='agent.rpm'
        else package='agent.deb'
    fi
    curl -H "Agent-Version-Control: on" $MANAGERURL/software/agent/${runningPlatform}${majorVersion}/${archType}/$package?tenantID=67712 -o /tmp/$package $CURLOPTIONS

    echo Installing agent package...
    rc=1
    if [[ $isRPM == 1 && -s /tmp/agent.rpm ]]; then
        rpm -ihv /tmp/agent.rpm
        rc=$?
    elif [[ -s /tmp/agent.deb ]]; then
        dpkg -i /tmp/agent.deb
        rc=$?
    else
        echo Failed to download the agent package. Please make sure the package is imported in the Deep Security Manager
        logger -t Failed to download the agent package. Please make sure the package is imported in the Deep Security Manager
        exit 1
    fi
    if [[ ${rc} != 0 ]]; then
        echo Failed to install the agent package
        logger -t Failed to install the agent package
        exit 1
    fi

echo Install the agent package successfully

sleep 15
sudo /opt/ds_agent/dsa_control -r
sudo /opt/ds_agent/dsa_control -a dsm://agents.deepsecurity.trendmicro.com:443/ "tenantID:954A3F6C-2C30-6359-C466-3A40FC6BFAEE" "token:60C7D34B-2D92-9510-305F-0AAC9861EA66" "policyid:859"
# /opt/ds_agent/dsa_control -a dsm://agents.deepsecurity.trendmicro.com:443/ "tenantID:954A3F6C-2C30-6359-C466-3A40FC6BFAEE" "token:60C7D34B-2D92-9510-305F-0AAC9861EA66"sysadmin@jenkins:~/ubuntu/aws_shellscript/aws_scriptautomation/aws_installsecagent/pat/gitlab-security-agents-2021/Installers$
sudo service ds_agent status
sudo service ds_agent start


