#!/bin/bash

echo "===================================================="
echo "GLOBE DSG INFRASTRUCTURE"
echo "WELCOME! DSG INSTALLATION OF SEC-AGENTS"
echo "===================================================="
echo

#INSTANCE_IDINFO=$(cat ec2instanceid.txt)

if [ -f /etc/os-release ]; then
    . /etc/os-release
    OS=$NAME
    VER=$VERSION_ID
    ID_OS=$ID
    OS_VER="${VER:0:2}"
    OS_VER_AMZ="${VER:0:4}"
    echo $NAME "$OS_VER"

    # Default Parameters
    WORKING_DIR=`pwd`
    SPLUNK_DIR="/opt/splunkforwarder"
    UNAME="admin"
    PWORD="9<70j>SS"
    DNSNAME=$1

    if [ $ID == "centos" ] && [ $OS_VER -eq "7" ]
    then
        # Move and extract Splunk
        cd $WORKING_DIR/Installers/
        sudo tar -xvzf $WORKING_DIR/Installers/splunkforwarder-8.0.6-152fb4b2bb96-Linux-x86_64.tgz -C /opt/
        cd $SPLUNK_DIR/bin
        sudo ./splunk start --accept-license --answer-yes --no-prompt
        sudo ./splunk enable boot-start 
        cd $SPLUNK_DIR/etc/system/local
        sudo cp $WORKING_DIR/Installers/SplunkInstaller-ISDP/deploymentclient.conf $SPLUNK_DIR/etc/system/local/
        sudo sed -i '7 i allowSslCompression = false' server.conf
        sudo sed -i '8 i sslVersions = tls' server.conf
        echo -e "...\n\n Completed Configuring deployment.conf and server.conf.....Restarting the Splunk Agent"
        cd $SPLUNK_DIR/bin/
        sudo ./splunk restart
        sudo ./splunk status

        # 2nd step of Splunk
        echo "Configuring the 2nd Step for Splunk"
        sudo sleep 3
        sudo cp -r $WORKING_DIR/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-isdp-linux-config $SPLUNK_DIR/etc/apps/
        sudo cp -r $WORKING_DIR/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-vpcendpoint-outputs $SPLUNK_DIR/etc/apps/
        cd $SPLUNK_DIR/etc/apps/TA-isdp-linux-config/bin/
        sudo chmod +x *.sh
        sudo cp $WORKING_DIR/user-seed.conf $SPLUNK_DIR/etc/system/local/
        echo "Completed the 2nd Step for Splunk.....Restarting the Splunk Agent"
        cd $SPLUNK_DIR/bin/
        ./splunk restart
        sudo service splunk start
        ./splunk status
    
    fi

    if [ $ID == "ubuntu" ] && [ $OS_VER == "16" ]
    then
        cd $WORKING_DIR/Installers
         
        sudo tar -xvzf $WORKING_DIR/Installers/splunkforwarder-8.0.6-152fb4b2bb96-Linux-x86_64.tgz -C /opt/
        cd $SPLUNK_DIR/bin
        sudo ./splunk start --accept-license --answer-yes --no-prompt
        sudo ./splunk enable boot-start 
        cd $SPLUNK_DIR/etc/system/local
        sudo cp $WORKING_DIR/Installers/SplunkInstaller-ISDP/deploymentclient.conf $SPLUNK_DIR/etc/system/local/
        sudo sed -i '7 i allowSslCompression = false' server.conf
        sudo sed -i '8 i sslVersions = tls' server.conf
        echo -e "...\n\n Completed Configuring deployment.conf and server.conf.....Restarting the Splunk Agent"
        cd $SPLUNK_DIR/bin/
        sudo ./splunk restart
        sudo ./splunk status
         
        echo "Configuring the 2nd Step for Splunk"
        sudo sleep 3
        sudo cp -r $WORKING_DIR/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-isdp-linux-config $SPLUNK_DIR/etc/apps/
        sudo cp -r $WORKING_DIR/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-vpcendpoint-outputs $SPLUNK_DIR/etc/apps/
        cd $SPLUNK_DIR/etc/apps/TA-isdp-linux-config/bin/
        sudo chmod +x *.sh
        sudo cp $WORKING_DIR/user-seed.conf $SPLUNK_DIR/etc/system/local/
        echo "Completed Configuring the 2nd Step for Splunk.....Restarting the Splunk Agent"
        cd $SPLUNK_DIR/bin/
        ./splunk restart
        sudo service splunk start
        ./splunk status
    fi

       if [ $ID == "ubuntu" ] && [ $OS_VER == "18" ]
    then
        cd $WORKING_DIR/Installers
         
        sudo tar -xvzf $WORKING_DIR/Installers/splunkforwarder-8.0.6-152fb4b2bb96-Linux-x86_64.tgz -C /opt/
        cd $SPLUNK_DIR/bin
        sudo ./splunk start --accept-license --answer-yes --no-prompt
        sudo ./splunk enable boot-start 
        cd $SPLUNK_DIR/etc/system/local
        sudo cp $WORKING_DIR/Installers/SplunkInstaller-ISDP/deploymentclient.conf $SPLUNK_DIR/etc/system/local/
        sudo sed -i '7 i allowSslCompression = false' server.conf
        sudo sed -i '8 i sslVersions = tls' server.conf
        echo -e "...\n\n Completed Configuring deployment.conf and server.conf.....Restarting the Splunk Agent"
        cd $SPLUNK_DIR/bin/
        sudo ./splunk restart
        sudo ./splunk status
         
        echo "Configuring the 2nd Step for Splunk"
        sudo sleep 3
        sudo cp -r $WORKING_DIR/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-isdp-linux-config $SPLUNK_DIR/etc/apps/
        sudo cp -r $WORKING_DIR/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-vpcendpoint-outputs $SPLUNK_DIR/etc/apps/
        cd $SPLUNK_DIR/etc/apps/TA-isdp-linux-config/bin/
        sudo chmod +x *.sh
        sudo cp $WORKING_DIR/user-seed.conf $SPLUNK_DIR/etc/system/local/
        echo "Completed Configuring the 2nd Step for Splunk.....Restarting the Splunk Agent"
        cd $SPLUNK_DIR/bin/
        ./splunk restart
        sudo service splunk start
        ./splunk status

    fi

           if [ $ID == "ubuntu" ] && [ $OS_VER == "20" ]
    then
        cd $WORKING_DIR/Installers
         
        sudo tar -xvzf $WORKING_DIR/Installers/splunkforwarder-8.0.6-152fb4b2bb96-Linux-x86_64.tgz -C /opt/
        cd $SPLUNK_DIR/bin
        sudo ./splunk start --accept-license --answer-yes --no-prompt
        sudo ./splunk enable boot-start 
        cd $SPLUNK_DIR/etc/system/local
        sudo cp $WORKING_DIR/Installers/SplunkInstaller-ISDP/deploymentclient.conf $SPLUNK_DIR/etc/system/local/
        sudo sed -i '7 i allowSslCompression = false' server.conf
        sudo sed -i '8 i sslVersions = tls' server.conf
        echo -e "...\n\n Completed Configuring deployment.conf and server.conf.....Restarting the Splunk Agent"
        cd $SPLUNK_DIR/bin/
        sudo ./splunk restart
        sudo ./splunk status
         
        echo "Configuring the 2nd Step for Splunk"
        sudo sleep 3
        sudo cp -r $WORKING_DIR/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-isdp-linux-config $SPLUNK_DIR/etc/apps/
        sudo cp -r $WORKING_DIR/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-vpcendpoint-outputs $SPLUNK_DIR/etc/apps/
        cd $SPLUNK_DIR/etc/apps/TA-isdp-linux-config/bin/
        sudo chmod +x *.sh
        sudo cp $WORKING_DIR/user-seed.conf $SPLUNK_DIR/etc/system/local/
        echo "Completed Configuring the 2nd Step for Splunk.....Restarting the Splunk Agent"
        cd $SPLUNK_DIR/bin/
        ./splunk restart
        sudo service splunk start
        ./splunk status
    fi


    if [ $ID == "ubuntu" ] && [ $OS_VER == "14" ]
    then
        cd $WORKING_DIR/Installers
         
        sudo tar -xvzf $WORKING_DIR/Installers/splunkforwarder-8.0.6-152fb4b2bb96-Linux-x86_64.tgz -C /opt/
        cd $SPLUNK_DIR/bin
        sudo ./splunk start --accept-license --answer-yes --no-prompt
        sudo ./splunk enable boot-start 
        cd $SPLUNK_DIR/etc/system/local
        sudo cp $WORKING_DIR/Installers/SplunkInstaller-ISDP/deploymentclient.conf $SPLUNK_DIR/etc/system/local/
        sudo sed -i '7 i allowSslCompression = false' server.conf
        sudo sed -i '8 i sslVersions = tls' server.conf
        echo -e "...\n\n Completed Configuring deployment.conf and server.conf.....Restarting the Splunk Agent"
        cd $SPLUNK_DIR/bin/
        sudo ./splunk restart
        sudo ./splunk status
         
        echo "Configuring the 2nd Step for Splunk"
        sudo sleep 3
        sudo cp -r $WORKING_DIR/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-isdp-linux-config $SPLUNK_DIR/etc/apps/
        sudo cp -r $WORKING_DIR/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-vpcendpoint-outputs $SPLUNK_DIR/etc/apps/
        cd $SPLUNK_DIR/etc/apps/TA-isdp-linux-config/bin/
        sudo chmod +x *.sh
        sudo cp $WORKING_DIR/user-seed.conf $SPLUNK_DIR/etc/system/local/
        echo "Completed Configuring the 2nd Step for Splunk.....Restarting the Splunk Agent"
        cd $SPLUNK_DIR/bin/
        ./splunk restart
        sudo service splunk start
        ./splunk status

    fi


    if [ $ID == "amzn" ] && [ $OS_VER_AMZ == "2018" ]
    then
        cd $WORKING_DIR/Installers
         
        sudo tar -xvzf $WORKING_DIR/Installers/splunkforwarder-8.0.6-152fb4b2bb96-Linux-x86_64.tgz -C /opt/
        cd $SPLUNK_DIR/bin
        sudo ./splunk start --accept-license --answer-yes --no-prompt
        sudo ./splunk enable boot-start 
        cd $SPLUNK_DIR/etc/system/local
        sudo cp $WORKING_DIR/Installers/SplunkInstaller-ISDP/deploymentclient.conf $SPLUNK_DIR/etc/system/local/
        sudo sed -i '7 i allowSslCompression = false' server.conf
        sudo sed -i '8 i sslVersions = tls' server.conf
        echo -e "...\n\n Completed Configuring deployment.conf and server.conf.....Restarting the Splunk Agent"
        cd $SPLUNK_DIR/bin/
        sudo ./splunk restart
        sudo ./splunk status
         
        echo "Configuring the 2nd Step for Splunk"
        sudo sleep 3
        sudo cp -r $WORKING_DIR/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-isdp-linux-config $SPLUNK_DIR/etc/apps/
        sudo cp -r $WORKING_DIR/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-vpcendpoint-outputs $SPLUNK_DIR/etc/apps/
        cd $SPLUNK_DIR/etc/apps/TA-isdp-linux-config/bin/
        sudo chmod +x *.sh
        sudo cp $WORKING_DIR/user-seed.conf $SPLUNK_DIR/etc/system/local/
        echo "Completed Configuring the 2nd Step for Splunk.....Restarting the Splunk Agent"
        cd $SPLUNK_DIR/bin/
        ./splunk restart
        sudo service splunk start
        ./splunk status
    fi

    if [ $ID == "amzn" ] && [ $OS_VER_AMZ == "2015" ]
    then
        cd $WORKING_DIR/Installers
         
        sudo tar -xvzf $WORKING_DIR/Installers/splunkforwarder-8.0.6-152fb4b2bb96-Linux-x86_64.tgz -C /opt/
        cd $SPLUNK_DIR/bin
        sudo ./splunk start --accept-license --answer-yes --no-prompt
        sudo ./splunk enable boot-start 
        cd $SPLUNK_DIR/etc/system/local
        sudo cp $WORKING_DIR/Installers/SplunkInstaller-ISDP/deploymentclient.conf $SPLUNK_DIR/etc/system/local/
        sudo sed -i '7 i allowSslCompression = false' server.conf
        sudo sed -i '8 i sslVersions = tls' server.conf
        echo -e "...\n\n Completed Configuring deployment.conf and server.conf.....Restarting the Splunk Agent"
        cd $SPLUNK_DIR/bin/
        sudo ./splunk restart
        sudo ./splunk status
         
        echo "Configuring the 2nd Step for Splunk"
        sudo sleep 3
        sudo cp -r $WORKING_DIR/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-isdp-linux-config $SPLUNK_DIR/etc/apps/
        sudo cp -r $WORKING_DIR/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-vpcendpoint-outputs $SPLUNK_DIR/etc/apps/
        cd $SPLUNK_DIR/etc/apps/TA-isdp-linux-config/bin/
        sudo chmod +x *.sh
        sudo cp $WORKING_DIR/user-seed.conf $SPLUNK_DIR/etc/system/local/
        echo "Completed Configuring the 2nd Step for Splunk.....Restarting the Splunk Agent"
        cd $SPLUNK_DIR/bin/
        ./splunk restart
        sudo service splunk start
        ./splunk status

    fi

    if [ $ID == "amzn" ] && [ $OS_VER_AMZ == "2017" ]
    then
        cd $WORKING_DIR/Installers
        
        sudo tar -xvzf $WORKING_DIR/Installers/splunkforwarder-8.0.6-152fb4b2bb96-Linux-x86_64.tgz -C /opt/
        cd $SPLUNK_DIR/bin
        sudo ./splunk start --accept-license --answer-yes --no-prompt
        sudo ./splunk enable boot-start 
        cd $SPLUNK_DIR/etc/system/local
        sudo cp $WORKING_DIR/Installers/SplunkInstaller-ISDP/deploymentclient.conf $SPLUNK_DIR/etc/system/local/
        sudo sed -i '7 i allowSslCompression = false' server.conf
        sudo sed -i '8 i sslVersions = tls' server.conf
        echo -e "...\n\n Completed Configuring deployment.conf and server.conf.....Restarting the Splunk Agent"
        cd $SPLUNK_DIR/bin/
        sudo ./splunk restart
        sudo ./splunk status
         
        echo "Configuring the 2nd Step for Splunk"
        sudo sleep 3
        sudo cp -r $WORKING_DIR/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-isdp-linux-config $SPLUNK_DIR/etc/apps/
        sudo cp -r $WORKING_DIR/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-vpcendpoint-outputs $SPLUNK_DIR/etc/apps/
        cd $SPLUNK_DIR/etc/apps/TA-isdp-linux-config/bin/
        sudo chmod +x *.sh
        sudo cp $WORKING_DIR/user-seed.conf $SPLUNK_DIR/etc/system/local/
        echo "Completed Configuring the 2nd Step for Splunk.....Restarting the Splunk Agent"
        cd $SPLUNK_DIR/bin/
        ./splunk restart
        sudo service splunk start
        ./splunk status

    fi

    if [ $ID == "amzn" ] && [ $OS_VER == "2" ]
    then
        cd $WORKING_DIR/Installers
         
        sudo tar -xvzf $WORKING_DIR/Installers/splunkforwarder-8.0.6-152fb4b2bb96-Linux-x86_64.tgz -C /opt/
        cd $SPLUNK_DIR/bin
        sudo ./splunk start --accept-license --answer-yes --no-prompt
        $SPLUNK_DIR/bin/splunk start
        sudo ./splunk enable boot-start 
        cd $SPLUNK_DIR/etc/system/local
        sudo cp $WORKING_DIR/Installers/SplunkInstaller-ISDP/deploymentclient.conf $SPLUNK_DIR/etc/system/local/
        sudo sed -i '7 i allowSslCompression = false' server.conf
        sudo sed -i '8 i sslVersions = tls' server.conf
        echo -e "...\n\n Completed Configuring deployment.conf and server.conf.....Restarting the Splunk Agent"
        cd $SPLUNK_DIR/bin/
        sudo ./splunk restart
        sudo ./splunk status
         
        echo "Configuring the 2nd Step for Splunk"
        sudo sleep 3
        sudo cp -r $WORKING_DIR/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-isdp-linux-config $SPLUNK_DIR/etc/apps/
        sudo cp -r $WORKING_DIR/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-vpcendpoint-outputs $SPLUNK_DIR/etc/apps/
        cd $SPLUNK_DIR/etc/apps/TA-isdp-linux-config/bin/
        sudo chmod +x *.sh
        sudo cp $WORKING_DIR/user-seed.conf $SPLUNK_DIR/etc/system/local/
        echo "Completed Configuring the 2nd Step for Splunk.....Restarting the Splunk Agent"
        cd $SPLUNK_DIR/bin/
        ./splunk restart
        sudo service splunk start
        ./splunk status


    fi

    if [ $ID == "rhel" ] && [ $OS_VER == "7" ]
    then
        cd $WORKING_DIR/Installers
         
        sudo tar -xvzf $WORKING_DIR/Installers/splunkforwarder-8.0.6-152fb4b2bb96-Linux-x86_64.tgz -C /opt/
        cd $SPLUNK_DIR/bin
        sudo ./splunk start --accept-license --answer-yes --no-prompt
        sudo ./splunk enable boot-start 
        cd $SPLUNK_DIR/etc/system/local
        sudo cp $WORKING_DIR/Installers/SplunkInstaller-ISDP/deploymentclient.conf $SPLUNK_DIR/etc/system/local/
        sudo sed -i '7 i allowSslCompression = false' server.conf
        sudo sed -i '8 i sslVersions = tls' server.conf
        echo -e "...\n\n Completed Configuring deployment.conf and server.conf.....Restarting the Splunk Agent"
        cd $SPLUNK_DIR/bin/
        sudo ./splunk restart
        sudo ./splunk status
         
        echo "Configuring the 2nd Step for Splunk"
        sudo sleep 3
        sudo cp -r $WORKING_DIR/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-isdp-linux-config $SPLUNK_DIR/etc/apps/
        sudo cp -r $WORKING_DIR/Installers/SplunkInstaller-ISDP/Splunk2ndStep-ISDP/TA-vpcendpoint-outputs $SPLUNK_DIR/etc/apps/
        cd $SPLUNK_DIR/etc/apps/TA-isdp-linux-config/bin/
        sudo chmod +x *.sh
        sudo cp $WORKING_DIR/user-seed.conf $SPLUNK_DIR/etc/system/local/
        echo "Completed Configuring the 2nd Step for Splunk.....Restarting the Splunk Agent"
        cd $SPLUNK_DIR/bin/
        ./splunk restart
        sudo service splunk start
        ./splunk status

    fi

fi

    echo
    echo "Completed..."