!!!README!!!

For Linux Systems/Servers:
- Must be root user first!
- Place both TA-isdp-linux-config and TA-vpcendpoint-outputs in /opt/splunkforwarder/etc/apps/ directory
- Change permissions of the script files
  # cd /opt/splunkforwarder/etc/apps/TA-isdp-linux-config/bin/
  # chmod +x *.sh
- Initiate Splunk service restart
  # cd /opt/splunkforwader/bin/
  # ./splunk restart

For Windows Systems/Servers:
- Must be admin user first!
- Place both TA-isdp-windows-config and TA-vpcendpoint-outputs in C:\Program Files\SplunkUniversalForwarder\etc\apps\ directory
- Run restart Splunk service via CMD and follow the ff commands:
- Open CMD and input the following commands
  # cd C:\Program Files\SplunkUniversalForwarder\bin\
  # splunk.exe restart
