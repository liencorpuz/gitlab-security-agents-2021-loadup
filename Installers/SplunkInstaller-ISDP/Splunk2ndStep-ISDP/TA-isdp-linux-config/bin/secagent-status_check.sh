#!/bin/bash
#######################################
### CREATED BY: RKR		 	        ###
### DATE CREATED: 2020-09-11        ###
### MODIFIED BY: RKR		        ###
### DATE MODIFIED: 2021-02-16       ###
#######################################
#START=$(date +%s)
os[1]="CENTOS";
os[2]="UBUNTU";
os[3]="RED HAT";
os[4]="SUSE";
os[5]="ORACLESERVER";
os[6]="EULEROS";
os[7]="AMAZON";
os[8]="ORACLE LINUX SERVER";

function getOS()
{
	os="$1"

	hctl_checker=`hostnamectl 2>&1`;
	hctl_checker_stat="$?";
	lsb_checker=`lsb_release -a 2>&1`;
	lsb_checker_stat="$?";
	etc_checker=`cat /etc/*-release 2>&1`;
	etc_checker_stat="$?";

	if [ "$hctl_checker_stat" -eq 0 ]; then 
		resOS=`hostnamectl | grep -w 'Operating System:' | sed "s/Operating System://" | sed -e 's/^[[:space:]]*//'`;
	elif [ "$lsb_checker_stat" -eq 0 ]; then 
		resOS=`lsb_release -si`;
	elif [ "$etc_checker_stat" -eq 0 ]; then 
		resOS=`cat /etc/*-release | grep -w NAME | sed -e "s/NAME=//" | tr -d \"`;
		if [ -z "$resOS" ]; then 
			resOS=`sed -n '1p' /etc/*release`;
		fi
	else
		resOS="Unable to get OS and Version";
	fi

	for((cnt=1; cnt<=${#os[@]}; cnt++));
    do
		grpOsRes=$(echo "${resOS}" | grep -io "${os[$cnt]}" 2>&1);
		#OsUpCase="${grpOsRes^^}"
		#OsUpCase="${grpOsRes^^}"
		OsUpCase=`echo $grpOsRes | tr '[a-z]' '[A-Z]' 2>&1`
		#echo "${OsUpCase}"
		
        if [ ! -z "${OsUpCase}" ]; then
            echo "${OsUpCase}";
        fi
    done

	echo "${OsUpCase}";
}
arrOS=$(getOS "${os[@]}")

####RhelandOtherAgents

function falcon_rhel_OthStatus()
{
cDate=$(date '+%Y-%m-%d %H:%M:%S');
name="CrowdStrike";
#Agent Version
        rpm_checker=`rpm -qi falcon-sensor 2>&1`;
        rpm_checker_stat="$?";

		if [ "$rpm_checker_stat" -eq 0 ]; then
                    version=`rpm -qi falcon-sensor | awk 'NR==2 {print$3}' | sed 's/|//' 2>&1`;
                    #runVer=`/opt/CrowdStrike/falconctl -g --version | sed "s/version =//" | sed -e 's/^[[:space:]]*//'`;
                    #version="Installed Version=${packVer}, Running Version=${runVer}";
        else
                version="Not Installed";
        fi
		
#Agent Status
        systemctl_checker=`systemctl status falcon-sensor 2> /dev/null`;
		secv_checker=`service falcon-sensor status 2> /dev/null`;

      if [[ "$rpm_checker_stat" -eq 0 ]] && [[ ! -z "$systemctl_checker" ]]; then
                agentStatus=`systemctl status falcon-sensor | grep -w 'Active:' | sed "s/Active://" | sed -e 's/^[[:space:]]*//' | sed 's/|//' 2>&1`;	
        elif [[ "$rpm_checker_stat" -eq 0 ]] && [[ ! -z "$secv_checker" ]]; then
                agentStatus=`service falcon-sensor status | sed 's/|//' 2>&1`;
		else
				agentStatus="Unable to get Agent Status"
	   fi

#Install Date
		rinstallD_checker=`rpm -q falcon-sensor --last 2>&1`;
        rinstallD_checker_stat="$?";

        if [ "$rinstallD_checker_stat" -eq 0 ]; then
                        installDate=`rpm -q falcon-sensor --last | tr -s [:blank:] | cut -d ' ' -f2- | sed 's/|//' 2>&1`;
        else
                installDate="Unable to get Install Date";
        fi
		
#Agent Utilization
		util_Checker=`ps -C falcon-sensor -o %cpu 2>&1`;
		util_Checker_stat="$?";
		
		if [ "$util_Checker_stat" -eq 0 ]; then
                cpuUtil=`ps -C falcon-sensor -o %cpu | awk 'NR==2 {print}' 2>&1`;
				memUtil=`ps -C falcon-sensor -o %mem | awk 'NR==2 {print}' 2>&1`;
				secUtil="CPU=$cpuUtil MEM=$memUtil";
        else
                secUtil="Unable to get Agent Utilization";
        fi

if [ "${version}" == "Not Installed" ]; then
        echo "${cDate} | ${name} | Not Installed | N/A | N/A | N/A";
else
echo "${cDate}|${name}|${agentStatus}|${version}|${installDate}|${secUtil}";
fi

}

function nessus_rhel_OthStatus()
{
cDate=$(date '+%Y-%m-%d %H:%M:%S');
name="Tenable Nessus Agent";
#Agent Version
        rpm_checker=`rpm -qi NessusAgent 2>&1`;
        rpm_checker_stat="$?";

		if [ "$rpm_checker_stat" -eq 0 ]; then
                    version=`rpm -qi NessusAgent | awk 'NR==2 {print$3}' | sed 's/|//' 2>&1`;
		else
                version="Not Installed";
        fi
		
#Agent Status		

		systemctl_checker=`systemctl status nessusagent 2> /dev/null`;
		secv_checker=`service nessusagent status 2> /dev/null`;

      if [[ "$rpm_checker_stat" -eq 0 ]] && [[ ! -z "$systemctl_checker" ]]; then
                agentStatus=`systemctl status nessusagent | grep -w 'Active:' | sed "s/Active://" | sed -e 's/^[[:space:]]*//' | sed 's/|//' 2>&1`;	
        elif [[ "$rpm_checker_stat" -eq 0 ]] && [[ ! -z "$secv_checker" ]]; then
                agentStatus=`service nessusagent status | sed 's/|//' 2>&1`;
		else
				agentStatus="Unable to get Agent Status"
	   fi

#Install Date

		rinstallD_checker=`rpm -q NessusAgent --last 2>&1`;
        rinstallD_checker_stat="$?";

        if [ "$rinstallD_checker_stat" -eq 0 ]; then
                        installDate=`rpm -q NessusAgent --last | tr -s [:blank:] | cut -d ' ' -f2- | sed 's/|//' 2>&1`;
		else
                installDate="Unable to get Install Date";
        fi
#Agent Utilization
		util_Checker=`ps -C nessusd -o %cpu 2>&1`;
		util_Checker_stat="$?";
		
		if [ "$util_Checker_stat" -eq 0 ]; then
                cpuUtil=`ps -C nessusd -o %cpu | awk 'NR==2 {print}' 2>&1`;
				memUtil=`ps -C nessusd -o %mem | awk 'NR==2 {print}' 2>&1`;
				secUtil="CPU=$cpuUtil MEM=$memUtil";
        else
                secUtil="Unable to get Agent Utilization";
        fi


if [ "${version}" == "Not Installed" ]; then
        echo "${cDate} | ${name} | Not Installed | N/A | N/A | N/A";
else
echo "${cDate}|${name}|${agentStatus}|${version}|${installDate}|${secUtil}";
fi

}

function gc_rhel_OthStatus()
{
cDate=$(date '+%Y-%m-%d %H:%M:%S');
name="GuardiCore";
#Agent Version
        rpm_checker=`rpm -qi gc-guest-agent 2>&1`;
        rpm_checker_stat="$?";

		if [ "$rpm_checker_stat" -eq 0 ]; then
                    version=`rpm -qi gc-guest-agent | awk 'NR==2 {print$3}' | sed 's/|//' 2>&1`;
        else
                version="Not Installed";
        fi
		
#Agent Status		

        systemctl_checker=`systemctl status gc-agent 2> /dev/null`;
		secv_checker=`service gc-agent status 2> /dev/null`;

      if [[ "$rpm_checker_stat" -eq 0 ]] && [[ ! -z "$systemctl_checker" ]]; then
                agentStatus=`systemctl status gc-agent | grep -w 'Active:' | sed "s/Active://" | sed -e 's/^[[:space:]]*//' | sed 's/|//' 2>&1`;	
        elif [[ "$rpm_checker_stat" -eq 0 ]] && [[ ! -z "$secv_checker" ]]; then
                agentStatus=`service gc-agent status | sed 's/|//' 2>&1`;
		else
				agentStatus="Unable to get Agent Status"
	   fi

#Install Date

		rinstallD_checker=`rpm -q gc-guest-agent --last 2>&1`;
        rinstallD_checker_stat="$?";

        if [ "$rinstallD_checker_stat" -eq 0 ]; then
                        installDate=`rpm -q gc-guest-agent --last | tr -s [:blank:] | cut -d ' ' -f2- | sed 's/|//' 2>&1`;
        else
                installDate="Unable to get Install Date";
        fi
#Agent Utilization
		util_Checker=`ps -C gc-guest-agent -o %cpu 2>&1`;
		util_Checker_stat="$?";
		
		if [ "$util_Checker_stat" -eq 0 ]; then
                cpuUtil=`ps -C gc-guest-agent -o %cpu | awk 'NR==2 {print}' 2>&1`;
				memUtil=`ps -C gc-guest-agent -o %mem | awk 'NR==2 {print}' 2>&1`;
				secUtil="CPU=$cpuUtil MEM=$memUtil";
        else
                secUtil="Unable to get Agent Utilization";
        fi


if [ "${version}" == "Not Installed" ]; then
        echo "${cDate}|${name}|Not Installed|N/A|N/A|N/A";
else
echo "${cDate}|${name}|${agentStatus}|${version}|${installDate}|${secUtil}";
fi

}

function ds_rhel_OthStatus()
{
cDate=$(date '+%Y-%m-%d %H:%M:%S');
name="DeepSec";
#Agent Version
        rpm_checker=`rpm -qi ds_agent 2>&1`;
        rpm_checker_stat="$?";

		if [ "$rpm_checker_stat" -eq 0 ]; then
                    version=`rpm -qi ds_agent | awk 'NR==2 {print$3}' | sed 's/|//' 2>&1`;
		else
                version="Not Installed";
        fi
		
#Agent Status		

        systemctl_checker=`systemctl status ds_agent 2> /dev/null`;
		secv_checker=`service ds_agent status 2> /dev/null`;

      if [[ "$rpm_checker_stat" -eq 0 ]] && [[ ! -z "$systemctl_checker" ]]; then
                agentStatus=`systemctl status ds_agent | grep -w 'Active:' | sed "s/Active://" | sed -e 's/^[[:space:]]*//' | sed 's/|//' 2>&1`;	
        elif [[ "$rpm_checker_stat" -eq 0 ]] && [[ ! -z "$secv_checker" ]]; then
                agentStatus=`service ds_agent status | sed 's/|//' 2>&1`;
		else
				agentStatus="Unable to get Agent Status"
	   fi

#Install Date

		rinstallD_checker=`rpm -q ds_agent --last 2>&1`;
        rinstallD_checker_stat="$?";

        if [ "$rinstallD_checker_stat" -eq 0 ]; then
                        installDate=`rpm -q ds_agent --last | tr -s [:blank:] | cut -d ' ' -f2- | sed 's/|//' 2>&1`;
		else
                installDate="Unable to get Install Date";
        fi
#Agent Utilization
		util_Checker=`ps -C ds_agent -o %cpu 2>&1`;
		util_Checker_stat="$?";
		
		if [ "$util_Checker_stat" -eq 0 ]; then
                cpuUtil=`ps -C ds_agent -o %cpu | awk 'NR==2 {print}' 2>&1`;
				memUtil=`ps -C ds_agent -o %mem | awk 'NR==2 {print}' 2>&1`;
				secUtil="CPU=$cpuUtil MEM=$memUtil";
        else
                secUtil="Unable to get Agent Utilization";
        fi


if [ "${version}" == "Not Installed" ]; then
        echo "${cDate} | ${name} | Not Installed | N/A | N/A | N/A";
else
echo "${cDate}|${name}|${agentStatus}|${version}|${installDate}|${secUtil}";
fi

}

function splunk_rhel_OthStatus()
{
cDate=$(date '+%Y-%m-%d %H:%M:%S');
name="Splunk";
#Agent Version
        rpm_checker=`/opt/splunkforwarder/bin/splunk --version 2>&1`;
        rpm_checker_stat="$?";

		if [ "$rpm_checker_stat" -eq 0 ]; then
                    version=`/opt/splunkforwarder/bin/splunk --version | cut -d ' ' -f4- | sed 's/|//' 2>&1`;
		else
                version="Not Installed";
        fi
		
#Agent Status		
		
        systemctl_checker=`systemctl status splunk 2> /dev/null`;
		secv_checker=`/opt/splunkforwarder/bin/splunk status 2> /dev/null`;

      if [[ "$rpm_checker_stat" -eq 0 ]] && [[ ! -z "$systemctl_checker" ]]; then
                agentStatus=`systemctl status splunk | grep -w 'Active:' | sed "s/Active://" | sed -e 's/^[[:space:]]*//' | sed 's/|//' 2> /dev/null`;	
        elif [[ "$rpm_checker_stat" -eq 0 ]] && [[ ! -z "$secv_checker" ]]; then
                agentStatus=`/opt/splunkforwarder/bin/splunk status	| grep splunkd | sed 's/|//' 2> /dev/null`;
		else
				agentStatus="Unable to get Agent Status"
	   fi

#Install Date

		rinstallD_checker=`date -r /opt/splunkforwarder/var/log/splunk 2>&1`;
        rinstallD_checker_stat="$?";

        if [ "$rinstallD_checker_stat" -eq 0 ]; then
                        installDate=`date -r /opt/splunkforwarder/var/log/splunk | sed 's/|//' 2>&1`;
        else
                installDate="Unable to get Install Date";
        fi
#Agent Utilization
		util_Checker=`ps -C splunkd -o %cpu 2>&1`;
		util_Checker_stat="$?";
		
		if [ "$util_Checker_stat" -eq 0 ]; then
                cpuUtil=`ps -C splunkd -o %cpu | awk 'NR==2 {print}' 2>&1`;
				memUtil=`ps -C splunkd -o %mem | awk 'NR==2 {print}' 2>&1`;
				secUtil="CPU=$cpuUtil MEM=$memUtil";
        else
                secUtil="Unable to get Agent Utilization";
        fi


if [ "${version}" == "Not Installed" ]; then
        echo "${cDate} | ${name} | Not Installed | N/A | N/A | N/A";
else
echo "${cDate}|${name}|${agentStatus}|${version}|${installDate}|${secUtil}";
fi

}

####UbuntuAgents

function falconUB_Status()
{
cDate=$(date '+%Y-%m-%d %H:%M:%S');
name="CrowdStrike";

#Agent Version
        dpkg_checker=`dpkg -l falcon-sensor 2>&1`;
        dpkg_checker_stat="$?";

		if [ "$dpkg_checker_stat" -eq 0 ]; then
				version=`dpkg -l falcon-sensor | grep -w 'falcon' | awk '{print $3}' | sed 's/|//' 2>&1`;
                #runVer=`/opt/CrowdStrike/falconctl -g --version | sed "s/version =//" | sed -e 's/^[[:space:]]*//' 2>&1`;
                #version="Installed Version=${packVer}, Running Version=${runVer}";
        else
                version="Not Installed";

        fi
		
#Agent Status
        systemctl_checker=`systemctl status falcon-sensor 2> /dev/null`;
		secv_checker=`service falcon-sensor status 2> /dev/null`;

      if [[ "$dpkg_checker_stat" -eq 0 ]] && [[ ! -z "$systemctl_checker" ]]; then
                agentStatus=`systemctl status falcon-sensor | grep -w 'Active:' | sed "s/Active://" | sed -e 's/^[[:space:]]*//' | sed 's/|//' 2>&1`;	
        elif [[ "$dpkg_checker_stat" -eq 0 ]] && [[ ! -z "$secv_checker" ]]; then
                agentStatus=`service falcon-sensor status | cut -d' ' -f3- | sed 's/|//' 2>&1`;
		else
				agentStatus="Unable to get Agent Status"
	   fi

#Install Date
        ubinstallD_checker=`date -r /opt/CrowdStrike 2>&1`;
        ubinstallD_checker_stat="$?";

        if [ "$ubinstallD_checker_stat" -eq 0 ]; then
                installDate=`date -r /opt/CrowdStrike | sed 's/|//' 2>&1`;
			else
                installDate="Unable to get Install Date";
        fi
#Agent Utilization
		util_Checker=`ps -C falcon-sensor -o %cpu 2>&1`;
		util_Checker_stat="$?";
		
		if [ "$util_Checker_stat" -eq 0 ]; then
                cpuUtil=`ps -C falcon-sensor -o %cpu | awk 'NR==2 {print}' 2>&1`;
				memUtil=`ps -C falcon-sensor -o %mem | awk 'NR==2 {print}' 2>&1`;
				secUtil="CPU=$cpuUtil MEM=$memUtil";
        else
                secUtil="Unable to get Agent Utilization";
        fi

if [ "${version}" == "Not Installed" ]; then
        echo "${cDate} | ${name} | Not Installed | N/A | N/A | N/A";
else
echo "${cDate}|${name}|${agentStatus}|${version}|${installDate}|${secUtil}";
fi

}

function nessusUB_Status()
{
cDate=$(date '+%Y-%m-%d %H:%M:%S');
name="Tenable Nessus Agent";

#Agent Version
        dpkg_checker=`dpkg -l nessusagent 2>&1`;
        dpkg_checker_stat="$?";

		if [ "$dpkg_checker_stat" -eq 0 ]; then
				version=`dpkg -l nessusagent | grep -w "nessusagent" | awk '{print $3}' | sed 's/|//' 2>&1`;
        else
                version="Not Installed";

        fi
		
#Agent Status
        systemctl_checker=`systemctl status nessusagent 2> /dev/null`;
		secv_checker=`service nessusagent status 2> /dev/null`;

      if [[ "$dpkg_checker_stat" -eq 0 ]] && [[ ! -z "$systemctl_checker" ]]; then
                agentStatus=`systemctl status nessusagent | grep -w 'Active:' | sed "s/Active://" | sed -e 's/^[[:space:]]*//' | sed 's/|//' 2>&1`;	
        elif [[ "$dpkg_checker_stat" -eq 0 ]] && [[ ! -z "$secv_checker" ]]; then
                agentStatus=`service nessusagent status | sed 's/|//' 2>&1`;
		else
				agentStatus="Unable to get Agent Status"
	   fi

#Install Date
        ubinstallD_checker=`date -r /opt/nessus_agent/lib/nessus/plugins 2>&1`;
        ubinstallD_checker_stat="$?";

        if [ "$ubinstallD_checker_stat" -eq 0 ]; then
                installDate=`date -r /opt/nessus_agent/lib/nessus/plugins | sed 's/|//' 2>&1`;
			else
                installDate="Unable to get Install Date";
        fi
#Agent Utilization
		util_Checker=`ps -C nessusd -o %cpu 2>&1`;
		util_Checker_stat="$?";
		
		if [ "$util_Checker_stat" -eq 0 ]; then
                cpuUtil=`ps -C nessusd -o %cpu | awk 'NR==2 {print}' 2>&1`;
				memUtil=`ps -C nessusd -o %mem | awk 'NR==2 {print}' 2>&1`;
				secUtil="CPU=$cpuUtil MEM=$memUtil";
        else
                secUtil="Unable to get Agent Utilization";
        fi

if [ "${version}" == "Not Installed" ]; then
        echo "${cDate} | ${name} | Not Installed | N/A | N/A | N/A";
else
echo "${cDate}|${name}|${agentStatus}|${version}|${installDate}|${secUtil}";
fi

}

function gcUB_Status()
{
cDate=$(date '+%Y-%m-%d %H:%M:%S');
name="GuardiCore";

#Agent Version
        dpkg_checker=`dpkg -l gc-guest-agent 2>&1`;
        dpkg_checker_stat="$?";

		if [ "$dpkg_checker_stat" -eq 0 ]; then
				version=`dpkg -l gc-guest-agent | grep -w "gc-guest-agent" | awk '{print $3}' | sed 's/|//' 2>&1`;
			else
                version="Not Installed";

        fi
		
#Agent Status
        systemctl_checker=`systemctl status gc-agent 2> /dev/null`;
		secv_checker=`service gc-agent status 2> /dev/null`;

      if [[ "$dpkg_checker_stat" -eq 0 ]] && [[ ! -z "$systemctl_checker" ]]; then
                agentStatus=`systemctl status gc-agent | grep -w 'Active:' | sed "s/Active://" | sed -e 's/^[[:space:]]*//' | sed 's/|//' 2>&1`;	
        elif [[ "$dpkg_checker_stat" -eq 0 ]] && [[ ! -z "$secv_checker" ]]; then
                agentStatus=`service gc-agent status | sed 's/|//' 2>&1`;
		else
				agentStatus="Unable to get Agent Status"
	   fi

#Install Date
        ubinstallD_checker=`date -r /var/lib/guardicore 2>&1`;
        ubinstallD_checker_stat="$?";

        if [ "$ubinstallD_checker_stat" -eq 0 ]; then
                installDate=`date -r /var/lib/guardicore | sed 's/|//' 2>&1`;
			else
                installDate="Unable to get Install Date";
        fi
#Agent Utilization
		util_Checker=`ps -C gc-guest-agent -o %cpu 2>&1`;
		util_Checker_stat="$?";
		
		if [ "$util_Checker_stat" -eq 0 ]; then
                cpuUtil=`ps -C gc-guest-agent -o %cpu | awk 'NR==2 {print}' 2>&1`;
				memUtil=`ps -C gc-guest-agent -o %mem | awk 'NR==2 {print}' 2>&1`;
				secUtil="CPU=$cpuUtil MEM=$memUtil";
        else
                secUtil="Unable to get Agent Utilization";
        fi

if [ "${version}" == "Not Installed" ]; then
        echo "${cDate} | ${name} | Not Installed | N/A | N/A | N/A";
else
echo "${cDate}|${name}|${agentStatus}|${version}|${installDate}|${secUtil}";
fi

}

function dsUB_Status()
{
cDate=$(date '+%Y-%m-%d %H:%M:%S');
name="DeepSec";

#Agent Version
        dpkg_checker=`dpkg -l ds-agent 2>&1`;
        dpkg_checker_stat="$?";

		if [ "$dpkg_checker_stat" -eq 0 ]; then
				version=`dpkg -l ds-agent | grep -w "ds-agent" | awk '{print $3}' | sed 's/|//' 2>&1`;
			else
                version="Not Installed";

        fi
		
#Agent Status
        systemctl_checker=`systemctl status ds_agent 2> /dev/null`;
		secv_checker=`service ds_agent status 2> /dev/null`;

      if [[ "$dpkg_checker_stat" -eq 0 ]] && [[ ! -z "$systemctl_checker" ]]; then
                agentStatus=`systemctl status ds_agent | grep -w 'Active:' | sed "s/Active://" | sed -e 's/^[[:space:]]*//' | sed 's/|//' 2>&1`;	
        elif [[ "$dpkg_checker_stat" -eq 0 ]] && [[ ! -z "$secv_checker" ]]; then
                agentStatus=`service ds_agent status | sed 's/|//' 2>&1`;
		else
				agentStatus="Unable to get Agent Status"
	   fi

#Install Date
        ubinstallD_checker=`date -r /opt/ds_agent/lib 2>&1`;
        ubinstallD_checker_stat="$?";

        if [ "$ubinstallD_checker_stat" -eq 0 ]; then
                installDate=`date -r /opt/ds_agent/lib | sed 's/|//' 2>&1`;
			else
                installDate="Unable to get Install Date";
        fi
#Agent Utilization
		util_Checker=`ps -C ds_agent -o %cpu 2>&1`;
		util_Checker_stat="$?";
		
		if [ "$util_Checker_stat" -eq 0 ]; then
                cpuUtil=`ps -C ds_agent -o %cpu | awk 'NR==2 {print}' 2>&1`;
				memUtil=`ps -C ds_agent -o %mem | awk 'NR==2 {print}' 2>&1`;
				secUtil="CPU=$cpuUtil MEM=$memUtil";
        else
                secUtil="Unable to get Agent Utilization";
        fi

if [ "${version}" == "Not Installed" ]; then
        echo "${cDate} | ${name} | Not Installed | N/A | N/A | N/A";
else
echo "${cDate}|${name}|${agentStatus}|${version}|${installDate}|${secUtil}";
fi

}

if [[ "${arrOS}" == "${os[1]}" ]]; then
#echo "CENTOS LOGIC HERE";
falcon_rhel_OthStatus
nessus_rhel_OthStatus
splunk_rhel_OthStatus
gc_rhel_OthStatus
ds_rhel_OthStatus
elif [[ "${arrOS}" == "${os[2]}" ]]; then
#echo "UBUNTU LOGIC HERE";
falconUB_Status
nessusUB_Status
splunk_rhel_OthStatus
gcUB_Status
dsUB_Status
elif [[ "${arrOS}" == "${os[3]}" ]]; then
#echo "RED HAT LOGIC HERE";
falcon_rhel_OthStatus
nessus_rhel_OthStatus
splunk_rhel_OthStatus
gc_rhel_OthStatus
ds_rhel_OthStatus
elif [[ "${arrOS}" == "${os[4]}" ]]; then
#echo "SUSE LOGIC HERE";
falcon_rhel_OthStatus
nessus_rhel_OthStatus
splunk_rhel_OthStatus
gc_rhel_OthStatus
ds_rhel_OthStatus
elif [[ "${arrOS}" == "${os[5]}" ]]; then
#echo "ORACLESERVER LOGIC HERE";
falcon_rhel_OthStatus
nessus_rhel_OthStatus
splunk_rhel_OthStatus
gc_rhel_OthStatus
ds_rhel_OthStatus
elif [[ "${arrOS}" == "${os[6]}" ]]; then
#	echo "EULEROS LOGIC HERE";
falcon_rhel_OthStatus
nessus_rhel_OthStatus
splunk_rhel_OthStatus
gc_rhel_OthStatus
ds_rhel_OthStatus
elif [[ "${arrOS}" == "${os[7]}" ]]; then
#	echo "AMAZON LOGIC HERE";
falcon_rhel_OthStatus
nessus_rhel_OthStatus
splunk_rhel_OthStatus
gc_rhel_OthStatus
ds_rhel_OthStatus
elif [[ "${arrOS}" == "${os[8]}" ]]; then
#echo "ORACLE LINUX SERVER LOGIC HERE";
falcon_rhel_OthStatus
nessus_rhel_OthStatus
splunk_rhel_OthStatus
gc_rhel_OthStatus
ds_rhel_OthStatus
fi

#END=$(date +%s)
#DIFF=$(( $END - $START ))
#echo "It took $DIFF seconds"
