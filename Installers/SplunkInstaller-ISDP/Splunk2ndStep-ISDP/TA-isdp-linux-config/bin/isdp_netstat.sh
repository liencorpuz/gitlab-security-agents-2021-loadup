#!/bin/bash

intervalInSec="900" # Set interval (duration) in seconds.

##################################################################################
############################## DO NOT TOUCH THIS PART ############################
##################################################################################
function main () {
        secs="$1"
        endTime=$(( $(date +%s) + secs )) # Calculate end time.

        while [ $(date +%s) -lt $endTime ]; do  # Loop until interval has elapsed.
          net_res+=$(netstat -anp | sort -n | uniq -c | grep "ESTABLISHED"& sleep 10)
        done

        new_var=$(echo "$net_res" | sort -n | uniq -c ) #Remove the dup data

        echo "$new_var" #Print trimmed data

        ps -ef | grep "netstat" | grep -v grep | awk '{print $2}' | xargs kill #Kill Netstat

}

cnts=$(rpm -qa 2>&1 | grep 'net-tools' | grep -v 'command not found')
debn=$(dpkg -l 2>&1 | grep 'net-tools' | grep -v 'command not found')

if [ -n "$cnts" ]; then
        main $intervalInSec
elif [ -n "$debn" ]; then
        main $intervalInSec
else
        echo 'Netstat not found.'
fi
