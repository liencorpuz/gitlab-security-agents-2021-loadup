#!/bin/bash
#######################################
### CREATED BY: ISDP-SOC DEV	    ###
### DATE CREATED: 2020-09-08        ###
### MODIFIED BY: RKR		        ###
### DATE MODIFIED: 2021-02-16       ###
#######################################
#START=$(date +%s)
os[1]="CENTOS";
os[2]="UBUNTU";
os[3]="RED HAT";
os[4]="SUSE";
os[5]="ORACLESERVER";
os[6]="EULEROS";
os[7]="AMAZON";
os[8]="ORACLE LINUX SERVER";

function getOS()
{
	os="$1"

	hctl_checker=`hostnamectl 2>&1`;
	hctl_checker_stat="$?";
	lsb_checker=`lsb_release -a 2>&1`;
	lsb_checker_stat="$?";
	etc_checker=`cat /etc/*-release 2>&1`;
	etc_checker_stat="$?";

	if [ "$hctl_checker_stat" -eq 0 ]; then 
		resOS=`hostnamectl | grep -w 'Operating System:' | sed "s/Operating System://" | sed -e 's/^[[:space:]]*//' 2>&1`;
	elif [ "$lsb_checker_stat" -eq 0 ]; then 
		resOS=`lsb_release -si 2>&1`;
	elif [ "$etc_checker_stat" -eq 0 ]; then 
		resOS=`cat /etc/*-release | grep -w NAME | sed -e "s/NAME=//" | tr -d \" 2>&1`;
		if [ -z "$resOS" ]; then 
			resOS=`sed -n '1p' /etc/*release 2>&1`;
		fi
	else
		resOS="Unable to get OS and Version";
	fi

	for((cnt=1; cnt<=${#os[@]}; cnt++));
    do
		grpOsRes=$(echo "${resOS}" | grep -io "${os[$cnt]}" 2>&1);
		#OsUpCase="${grpOsRes^^}"
		#OsUpCase="${grpOsRes^^}"
		OsUpCase=`echo $grpOsRes | tr '[a-z]' '[A-Z]' 2>&1`;
		#echo "${OsUpCase}"
		
        if [ ! -z "${OsUpCase}" ]; then
            echo "${OsUpCase}";
        fi
    done

	echo "${OsUpCase}";
}
arrOS=$(getOS "${os[@]}")

#echo "${arrOS}"

function rhelInfo()
{
cDate=$(date '+%Y-%m-%d %H:%M:%S');
#hostname=`hostname`;
#Hostname
	hostname_checker=`hostname 2>&1`;
	hostname_checker_stat="$?";
	
	if [ "$hostname_checker_stat" -eq 0 ]; then
			hostname=`hostname`;
	else
		hostname="Unable to get Hostname";
	fi
	
#OperatingSystem
	hostnamectl_checker=`hostnamectl 2>&1`;
	hostnamectl_checker_stat="$?";
	lsb_checker=`lsb_release -a 2>&1`; 
	lsb_checker_stat="$?";
	etc_checker=`cat /etc/*-release | grep -w NAME 2>&1`;
	etc_checker_stat="$?";

	if [ "$hostnamectl_checker_stat" -eq 0 ]; then 
			operatingSystem=`hostnamectl | grep "Operating System" | cut -d ' ' -f5- | sed 's/|//' 2>&1`;
		elif [ "$etc_checker_stat" -eq 0 ]; then
			resOS=`cat /etc/*-release | grep -w NAME | sed -e "s/NAME=//" | tr -d \" | sed 's/|//' 2>&1`;
			resVR=`cat /etc/*-release | grep -w VERSION | sed -e "s/VERSION=//" | tr -d \" | sed 's/|//' 2>&1`;
			operatingSystem="$resOS $resVR";
		elif [ "$lsb_checker_stat" -eq 0 ]; then 
			resOS=`lsb_release -si | sed 's/|//'`;
			resVR=`lsb_release -sr | sed 's/|//'`;
			operatingSystem="$resOS $resVR";

			if [ -z "$resOS" ]; then 
				operatingSystem=`sed -n '1p' /etc/*release | sed 's/|//' 2>&1`;
			fi
			
	else
		operatingSystem="Unable to get Operating System";
	fi
	
	kernel=`uname -r | sed 's/|//' 2>&1`;
	
#manufacturer=`cat /sys/class/dmi/id/chassis_vendor 2>&1`;
#Manufacturer
	manufacturer_checker=`cat /sys/class/dmi/id/chassis_vendor  2>&1`;
	manufacturer_checker_stat="$?";
	manudmi_checker=`dmidecode -s baseboard-manufacturer  2>&1`;
	manudmi_checker_stat="$?";
	
	if [ "$manufacturer_checker_stat" -eq 0 ]; then
			manufacturer=`cat /sys/class/dmi/id/chassis_vendor | sed 's/|//' 2>&1`;
		elif [ "$manudmi_checker_stat" -eq 0 ]; then
			manufacturer=`dmidecode -s baseboard-manufacturer | sed 's/|//' 2>&1`;
	else
		manufacturer="Unable to get Manufacturer";
	fi
	
#installDate=`rpm -q basesystem --qf '%{installtime:date}\n'`;
#Install Date	
	basesystem_checker=`rpm -q basesystem 2>&1`;
	basesystem_checker_stat="$?";
	tune2fs_checker=`fs=$(df / | tail -1 | cut -f1 -d' ') && tune2fs -l $fs 2>&1`;
	tune2fs_checker_stat="$?";
	rpmlast_checker=`rpm -qa --last 2>&1`;
	rpmlast_checker_stat="$?";
	
	if [ "$basesystem_checker_stat" -eq 0 ]; then 
			installDate=`rpm -q basesystem --qf '%{installtime:date}\n' | sed 's/|//' 2>&1`;
		elif [ "$tune2fs_checker_stat" -eq 0 ]; then 
			installDate=`fs=$(df / | tail -1 | cut -f1 -d' ') && tune2fs -l $fs | grep 'Filesystem created:' | sed "s/Filesystem created://" | sed -e 's/^[[:space:]]*//' | sed 's/|//' 2>&1`;
		elif [ "$rpmlast_checker_stat" -eq 0 ]; then 
			installDate=`rpm -qa --last | tail -1 | tr -s [:blank:] | cut -d ' ' -f2- | sed 's/|//' 2>&1`;
	else
		installDate="Unable to get Install Date";
	fi

#systemBootTime=`who -b | grep -w 'system boot' | sed "s/system boot//" | sed -e 's/^[[:space:]]*//'`;
#System Boot Time
	systemBoot_checker=`who -b | grep -w 'system boot' | sed "s/system boot//" | sed -e 's/^[[:space:]]*//'  2>&1`;
	systemBoot_checker_stat="$?";
	
	if [ "$systemBoot_checker_stat" -eq 0 ]; then
			systemBootTime=`who -b | grep -w 'system boot' | sed "s/system boot//" | sed -e 's/^[[:space:]]*//' | sed 's/|//' 2>&1`;
	else
		systemBootTime="Unable to get System Boot Time";
	fi
	
#systemManufacturer=`dmidecode -s system-manufacturer`;
#System Manufacturer
	systemManu_checker=`dmidecode -s system-manufacturer  2>&1`;
	systemManu_checker_stat="$?";
	
	if [ "$systemManu_checker_stat" -eq 0 ]; then
			systemManufacturer=`dmidecode -s system-manufacturer | sed 's/|//' 2>&1`;
	else
		systemManufacturer="Unable to get System Manufacturer";
	fi
	
#systemModel=`dmidecode -s system-product-name`;
#System Model
	systemMod_checker=`dmidecode -s system-product-name  2>&1`;
	systemMod_checker_stat="$?";
	
	if [ "$systemMod_checker_stat" -eq 0 ]; then
			systemModel=`dmidecode -s system-product-name | sed 's/|//' 2>&1`;
	else
		systemModel="Unable to get System Model";
	fi
	
#systemType=`arch`;
#System Type
	systemArch_checker=`arch  2>&1`;
	systemArch_checker_stat="$?";
	
	if [ "$systemArch_checker_stat" -eq 0 ]; then
			systemType=`arch 2>&1`;
	else
		systemType="Unable to get System Type";
	fi
	
#domain=`domainname`;
#Domain Name
	d_checker=`domainname 2>&1`;
	d_checker_stat="$?";
	
	if [ "$d_checker_stat" -eq 0 ]; then 
			domain=`domainname  2>&1`;
	else
		Domain="unable to get Domain Name";
	fi
	
#NetworkCard=`lspci | egrep -i --color 'network|ethernet' | cut -d ' ' -f2- 2>&1`;
#Network Card, IP Address, and MAC Address

netlspci_checker=`lspci 2>/dev/null`;
netlspci_checker_stat="$?";

if [ "${netlspci_checker_stat}" -eq 0 ]; then
		netCount=`lspci | egrep -i --color 'network|ethernet' | wc -l 2>/dev/null`;
	else
		netCount="0";
fi

networkCardRes=();
ipAddrRes=();
macaddresRes=();

if [[ "${netCount}" -ge 1 ]]; then

for (( ctr=1; ctr<=$netCount; ctr++ ))
do
    netcard_count=`lspci | egrep -i --color 'network|ethernet' | cut -d ' ' -f2- | awk 'NR=='"$ctr"' {print}' | wc -l 2>&1`;
		if [ $netcard_count -eq 1 ]; then
#Network Card
			ncard=`lspci | egrep -i --color 'network|ethernet' | cut -d ' ' -f4- | awk 'NR=='"$ctr"' {print}' 2>&1`;

#IP Address			
			ipshow_checker=`ip address show | grep "inet " | grep "brd " | grep -v "host" | awk 'NR=='"$ctr"' {print}' 2>&1`;
			ipshow_checker_stat="$?";
			ifconfig_checker=`ifconfig | grep "inet addr:" | grep "Bcast:" | awk 'NR=='"$ctr"' {print}' 2>&1`;
			ifconfig_checker_stat="$?";
			ifconfig1_checker=`ifconfig | grep "inet " | grep "broadcast " | sed "s/inet//" | awk '{print $1}' | awk 'NR=='"$ctr"' {print}' 2>&1`;
			ifconfig1_checker_stat="$?";
	
			if [[ "$ipshow_checker_stat" -eq 0  && ! -z "$ipshow_checker" ]]; then
					ippAddr=`ip address show | grep "inet " | grep "brd " | grep -v "host" | sed "s/inet//" | awk -F"/" '{print $1}' | sed -e 's/^[[:space:]]*//' | awk 'NR=='"$ctr"' {print}' 2>&1`;
				elif	[[ "$ifconfig1_checker_stat" -eq 0  && ! -z "$ifconfig1_checker" ]]; then
					ippAddr=`ifconfig | grep "inet " | grep "broadcast " | sed "s/inet//" | awk '{print $1}' | awk 'NR=='"$ctr"' {print}' 2>&1`;
				elif	[[ "$ifconfig_checker_stat" -eq 0  && ! -z "$ifconfig_checker" ]]; then
					ippAddr=`ifconfig | grep "inet addr:" | grep "Bcast:" | sed "s/inet addr://" | awk '{print $1}' | awk 'NR=='"$ctr"' {print}' 2>&1`;
			else
					ippAddr="N/A";
			fi
			
#MAC Address
			mcaddr_checker=`ip link show | grep "link/ether " | awk '{print $2}' | awk 'NR=='"$ctr"' {print}' 2>&1`;
			mcaddr_checker_stat="$?";
			mcaddr2_checker=`ifconfig | grep "ether " | sed "s/ether//" | awk '{print $1}' | awk 'NR=='"$ctr"' {print}' 2>&1`;
			mcaddr2_checker_stat="$?";
			hwAddr_checker=`ifconfig | grep "HWaddr " | sed "s/HWaddr//" | awk '{print $1}' | awk 'NR=='"$ctr"' {print}' 2>&1`;
			hwAddr_checker_stat="$?";	

			if [[ "$mcaddr_checker_stat" -eq 0  && ! -z "$mcaddr_checker" ]]; then
					mcaddr=`ip link show | grep "link/ether " | awk '{print $2}' | awk 'NR=='"$ctr"' {print}' 2>&1`;
				elif [[ "$mcaddr2_checker_stat" -eq 0  && ! -z "$mcaddr2_checker" ]]; then
					mcaddr=`ifconfig | grep "ether " | sed "s/ether//" | awk '{print $1}' | awk 'NR=='"$ctr"' {print}' 2>&1`;
				elif [[ "$hwAddr_checker_stat" -eq 0 && ! -z "$hwAddr_checker" ]]; then
					mcaddr=`ifconfig | grep "HWaddr " | sed "s/HWaddr//" | awk '{print $1}' | awk 'NR=='"$ctr"' {print}' 2>&1`;
			else
					mcaddr="N/A";
			fi
			
			nl=$',';		
			
			networkCardRes+="${ncard}${nl}"
			ipAddrRes+="${ippAddr}${nl}"
			macaddressRes+="${mcaddr}${nl}"
			#networkCard+="$(echo "$netCard" | sed 's/,$//')"
		fi;
done
	networkCard=`echo "${networkCardRes[@]}" | sed 's/,$//g'`
	ipAddr=`echo "${ipAddrRes[@]}" | sed 's/,$//g'`
	macaddress=`echo "${macaddressRes[@]}" | sed 's/,$//g'`

else
	
	networkHW_checker=`lshw -class network 2>/dev/null`;
	networkHW_checker_stat="$?";
	
			if [ "${networkHW_checker_stat}" -eq 0 ]; then
					network_prod=`lshw -class network | grep -w 'product:' | sed "s/product://" | sed -e 's/^[[:space:]]*//'`;
					network_vend=`lshw -class network | grep -w 'vendor:' | sed "s/vendor://" | sed -e 's/^[[:space:]]*//'`;
					networkCard="${network_vend} ${network_prod}"
			else		
					networkCard="N/A";
			fi;
	
	ipAddr=`hostname -I | awk '{print$1}'`;
	macaddressbond=`ip address show | grep "${ipAddr}" | awk '{print$7}' 2> /dev/null`;
	mcaddr_checker=`ip link show "${macaddressbond}" | grep "link/ether" | wc -l 2> /dev/null`;

			if [ "${mcaddr_checker}" -eq 1 ]; then
					macaddress=`ip link show "${macaddressbond}" | grep "link/ether " | awk '{print $2}' 2>&1`;
			else
					macaddress="N/A";
			fi;
fi;	

	echo "${cDate}|${hostname}|${operatingSystem}|${kernel}|${manufacturer}|${installDate}|${systemBootTime}|${systemManufacturer}|${systemModel}|${systemType}|${domain}|${networkCard[@]}|${ipAddr[@]}|${macaddress[@]}";

}


function centosInfo()
{
cDate=$(date '+%Y-%m-%d %H:%M:%S');
#Hostname=`hostname`;
#Hostname
	hostname_checker=`hostname 2>&1`;
	hostname_checker_stat="$?";
	
	if [ "$hostname_checker_stat" -eq 0 ]; then
			hostname=`hostname`;
	else
		hostname="Unable to get Hostname";
	fi

#OperatingSystem
	hostnamectl_checker=`hostnamectl 2>&1`;
	hostnamectl_checker_stat="$?";
	lsb_checker=`lsb_release -a 2>&1`; 
	lsb_checker_stat="$?";
	etc_checker=` `;
	etc_checker_stat="$?";

	if [ "$hostnamectl_checker_stat" -eq 0 ]; then 
			operatingSystem=`hostnamectl | grep "Operating System" | cut -d ' ' -f5- | sed 's/|//' 2>&1`;
		elif [ "$lsb_checker_stat" -eq 0 ]; then 
			resOS=`lsb_release -si | sed 's/|//' 2>&1`;
			resVR=`lsb_release -sr | sed 's/|//' 2>&1`;
			operatingSystem="$resOS $resVR";
		elif [ "$etc_checker_stat" -eq 0 ]; then 
			resOS=`cat /etc/*-release | grep -w NAME | sed -e "s/NAME=//" | tr -d \" | sed 's/|//' 2>&1`;
			resVR=`cat /etc/*-release | grep -w VERSION | sed -e "s/VERSION=//" | tr -d \" | sed 's/|//' 2>&1`;
			operatingSystem="$resOS $resVR";

			if [ -z "$resOS" ]; then 
				operatingSystem=`sed -n '1p' /etc/*release | sed 's/|//' 2>&1`;
			fi
			
	else
		operatingSystem="Unable to get Operating System";
	fi
	
	kernel=`uname -r 2>&1`;
#manufacturer=`cat /sys/class/dmi/id/chassis_vendor 2>&1`;
#Manufacturer
	manufacturer_checker=`cat /sys/class/dmi/id/chassis_vendor  2>&1`;
	manufacturer_checker_stat="$?";
	manudmi_checker=`dmidecode -s baseboard-manufacturer  2>&1`;
	manudmi_checker_stat="$?";
	
	if [ "$manufacturer_checker_stat" -eq 0 ]; then
			manufacturer=`cat /sys/class/dmi/id/chassis_vendor | sed 's/|//' 2>&1`;
		elif [ "$manudmi_checker_stat" -eq 0 ]; then
			manufacturer=`dmidecode -s baseboard-manufacturer | sed 's/|//' 2>&1`;
	else
		manufacturer="Unable to get Manufacturer";
	fi

#InstallDate=`rpm -q basesystem --qf '%{installtime:date}\n'`;
#Install Date	
	basesystem_checker=`rpm -q basesystem 2>&1`;
	basesystem_checker_stat="$?";
	tune2fs_checker=`fs=$(df / | tail -1 | cut -f1 -d' ') && tune2fs -l $fs 2>&1`;
	tune2fs_checker_stat="$?";
	rpmlast_checker=`rpm -qa --last 2>&1`;
	rpmlast_checker_stat="$?";
	
	if [ "$basesystem_checker_stat" -eq 0 ]; then 
			installDate=`rpm -q basesystem --qf '%{installtime:date}\n' | sed 's/|//' 2>&1`;
		elif [ "$tune2fs_checker_stat" -eq 0 ]; then 
			installDate=`fs=$(df / | tail -1 | cut -f1 -d' ') && tune2fs -l $fs | grep 'Filesystem created:' | sed "s/Filesystem created://" | sed -e 's/^[[:space:]]*//' | sed 's/|//' 2>&1`;
		elif [ "$rpmlast_checker_stat" -eq 0 ]; then 
			installDate=`rpm -qa --last | tail -1 | tr -s [:blank:] | cut -d ' ' -f2- | sed 's/|//' 2>&1`;
	else
		installDate="Unable to get Install Date";
	fi
	
#SystemBootTime=`who -b | grep -w 'system boot' | sed "s/system boot//" | sed -e 's/^[[:space:]]*//'`;
#System Boot Time
	systemBoot_checker=`who -b | grep -w 'system boot' | sed "s/system boot//" | sed -e 's/^[[:space:]]*//'  2>&1`;
	systemBoot_checker_stat="$?";
	
	if [ "$systemBoot_checker_stat" -eq 0 ]; then
			systemBootTime=`who -b | grep -w 'system boot' | sed "s/system boot//" | sed -e 's/^[[:space:]]*//' | sed 's/|//' 2>&1`;
	else
		systemBootTime="Unable to get System Boot Time";
	fi
	
#SystemManufacturer=`dmidecode -s system-manufacturer`;
#System Manufacturer
	systemManu_checker=`dmidecode -s system-manufacturer  2>&1`;
	systemManu_checker_stat="$?";
	
	if [ "$systemManu_checker_stat" -eq 0 ]; then
			systemManufacturer=`dmidecode -s system-manufacturer | sed 's/|//' 2>&1`;
	else
		systemManufacturer="Unable to get System Manufacturer";
	fi
	
#SystemModel=`dmidecode -s system-product-name`;
#System Model
	systemMod_checker=`dmidecode -s system-product-name  2>&1`;
	systemMod_checker_stat="$?";
	
	if [ "$systemMod_checker_stat" -eq 0 ]; then
			systemModel=`dmidecode -s system-product-name | sed 's/|//' 2>&1`;
	else
		systemModel="Unable to get System Model";
	fi

#systemType=`arch`;
#System Type
	systemArch_checker=`arch  2>&1`;
	systemArch_checker_stat="$?";
	
	if [ "$systemArch_checker_stat" -eq 0 ]; then
			systemType=`arch 2>&1`;
	else
		systemType="Unable to get System Type";
	fi

#domain=`domainname`;
#Domain Name
	d_checker=`domainname 2>&1`;
	d_checker_stat="$?";
	
	if [ "$d_checker_stat" -eq 0 ]; then 
			domain=`domainname 2>&1`;
	else
		Domain="unable to get Domain Name";
	fi

	
#NetworkCard=`lspci | egrep -i --color 'network|ethernet' | cut -d ' ' -f2-`;
#Network Card, IP Address, and MAC Address
netlspci_checker=`lspci 2>/dev/null`;
netlspci_checker_stat="$?";

if [ "${netlspci_checker_stat}" -eq 0 ]; then
		netCount=`lspci | egrep -i --color 'network|ethernet' | wc -l 2>/dev/null`;
	else
		netCount="0";
fi

networkCardRes=();
ipAddrRes=();
macaddresRes=();

if [ "$netCount" -ge 1 ]; then

for (( ctr=1; ctr<=$netCount; ctr++ ))
do
    netcard_count=`lspci | egrep -i --color 'network|ethernet' | cut -d ' ' -f2- | awk 'NR=='"$ctr"' {print}' | wc -l 2>&1`;
		if [ $netcard_count -eq 1 ]; then
#Network Card
			ncard=`lspci | egrep -i --color 'network|ethernet' | cut -d ' ' -f4- | awk 'NR=='"$ctr"' {print}' 2>&1`;

#IP Address			
			ipshow_checker=`ip address show | grep "inet " | grep "brd " | grep -v "host" | awk 'NR=='"$ctr"' {print}' 2>&1`;
			ipshow_checker_stat="$?";
			ifconfig_checker=`ifconfig | grep "inet addr:" | grep "Bcast:" | awk 'NR=='"$ctr"' {print}' 2>&1`;
			ifconfig_checker_stat="$?";
			ifconfig1_checker=`ifconfig | grep "inet " | grep "broadcast " | sed "s/inet//" | awk '{print $1}' | awk 'NR=='"$ctr"' {print}' 2>&1`;
			ifconfig1_checker_stat="$?";
	
			if [[ "$ipshow_checker_stat" -eq 0  && ! -z "$ipshow_checker" ]]; then
					ippAddr=`ip address show | grep "inet " | grep "brd " | grep -v "host" | sed "s/inet//" | awk -F"/" '{print $1}' | sed -e 's/^[[:space:]]*//' | awk 'NR=='"$ctr"' {print}' 2>&1`;
				elif	[[ "$ifconfig1_checker_stat" -eq 0  && ! -z "$ifconfig1_checker" ]]; then
					ippAddr=`ifconfig | grep "inet " | grep "broadcast " | sed "s/inet//" | awk '{print $1}' | awk 'NR=='"$ctr"' {print}' 2>&1`;
				elif	[[ "$ifconfig_checker_stat" -eq 0  && ! -z "$ifconfig_checker" ]]; then
					ippAddr=`ifconfig | grep "inet addr:" | grep "Bcast:" | sed "s/inet addr://" | awk '{print $1}' | awk 'NR=='"$ctr"' {print}' 2>&1`;
			else
					ippAddr="N/A";
			fi
			
#MAC Address
			mcaddr_checker=`ip link show | grep "link/ether " | awk '{print $2}' | awk 'NR=='"$ctr"' {print}' 2>&1`;
			mcaddr_checker_stat="$?";
			mcaddr2_checker=`ifconfig | grep "ether " | sed "s/ether//" | awk '{print $1}' | awk 'NR=='"$ctr"' {print}' 2>&1`;
			mcaddr2_checker_stat="$?";
			hwAddr_checker=`ifconfig | grep "HWaddr " | sed "s/HWaddr//" | awk '{print $1}' | awk 'NR=='"$ctr"' {print}' 2>&1`;
			hwAddr_checker_stat="$?";	

			if [[ "$mcaddr_checker_stat" -eq 0  && ! -z "$mcaddr_checker" ]]; then
					mcaddr=`ip link show | grep "link/ether " | awk '{print $2}' | awk 'NR=='"$ctr"' {print}' 2>&1`;
				elif [[ "$mcaddr2_checker_stat" -eq 0  && ! -z "$mcaddr2_checker" ]]; then
					mcaddr=`ifconfig | grep "ether " | sed "s/ether//" | awk '{print $1}' | awk 'NR=='"$ctr"' {print}' 2>&1`;
				elif [[ "$hwAddr_checker_stat" -eq 0 && ! -z "$hwAddr_checker" ]]; then
					mcaddr=`ifconfig | grep "HWaddr " | sed "s/HWaddr//" | awk '{print $1}' | awk 'NR=='"$ctr"' {print}' 2>&1`;
			else
					mcaddr="N/A";
			fi
			
			nl=$',';		
			
			networkCardRes+="${ncard}${nl}"
			ipAddrRes+="${ippAddr}${nl}"
			macaddressRes+="${mcaddr}${nl}"
			#networkCard+="$(echo "$netCard" | sed 's/,$//')"
		fi;
done
	networkCard=`echo "${networkCardRes[@]}" | sed 's/,$//g'`
	ipAddr=`echo "${ipAddrRes[@]}" | sed 's/,$//g'`
	macaddress=`echo "${macaddressRes[@]}" | sed 's/,$//g'`

else
	networkHW_checker=`lshw -class network 2>/dev/null`;
	networkHW_checker_stat="$?";
	
			if [ "${networkHW_checker_stat}" -eq 0 ]; then
					network_prod=`lshw -class network | grep -w 'product:' | sed "s/product://" | sed -e 's/^[[:space:]]*//'`;
					network_vend=`lshw -class network | grep -w 'vendor:' | sed "s/vendor://" | sed -e 's/^[[:space:]]*//'`;
					networkCard="${network_vend} ${network_prod}"
			else		
					networkCard="N/A";
			fi;
	
	ipAddr=`hostname -I | awk '{print$1}'`;
	macaddressbond=`ip address show | grep "${ipAddr}" | awk '{print$7}' 2> /dev/null`;
	mcaddr_checker=`ip link show "${macaddressbond}" | grep "link/ether" | wc -l 2> /dev/null`;

			if [ "${mcaddr_checker}" -eq 1 ]; then
					macaddress=`ip link show "${macaddressbond}" | grep "link/ether " | awk '{print $2}' 2>&1`;
			else
					macaddress="N/A";
			fi;
fi;	

	echo "${cDate}|${hostname}|${operatingSystem}|${kernel}|${manufacturer}|${installDate}|${systemBootTime}|${systemManufacturer}|${systemModel}|${systemType}|${domain}|${networkCard[@]}|${ipAddr[@]}|${macaddress[@]}";

}

function ubuntuInfo()
{
cDate=$(date '+%Y-%m-%d %H:%M:%S');
#hostname=`hostname`;
#Hostname
	hostname_checker=`hostname 2>&1`;
	hostname_checker_stat="$?";
	
	if [ "$hostname_checker_stat" -eq 0 ]; then
			hostname=`hostname 2>&1`;
	else
		hostname="Unable to get Hostname";
	fi
	
#OperatingSystem
	hostnamectl_checker=`hostnamectl 2>&1`;
	hostnamectl_checker_stat="$?";
	lsb_checker=`lsb_release -a 2>&1`; 
	lsb_checker_stat="$?";
	etc_checker=` `;
	etc_checker_stat="$?";

	if [ "$hostnamectl_checker_stat" -eq 0 ]; then 
			operatingSystem=`hostnamectl | grep "Operating System" | cut -d ' ' -f5- | sed 's/|//' 2>&1`;
		elif [ "$lsb_checker_stat" -eq 0 ]; then 
			resOS=`lsb_release -si | sed 's/|//' 2>&1`;
			resVR=`lsb_release -sr | sed 's/|//' 2>&1`;
			operatingSystem="$resOS $resVR";
		elif [ "$etc_checker_stat" -eq 0 ]; then 
			resOS=`cat /etc/*-release | grep -w NAME | sed -e "s/NAME=//" | tr -d \" | sed 's/|//' 2>&1`;
			resVR=`cat /etc/*-release | grep -w VERSION | sed -e "s/VERSION=//" | tr -d \" | sed 's/|//' 2>&1`;
			operatingSystem="$resOS $resVR";

			if [ -z "$resOS" ]; then 
				operatingSystem=`sed -n '1p' /etc/*release | sed 's/|//' 2>&1`;
			fi			
	else
		operatingSystem="Unable to get Operating System";
	fi
	
#kernel=`uname -r`;
#Kernel	
	kernel_checker=`uname -r  2>&1`;
	kernel_checker_stat="$?";
	
	if [ "$kernel_checker_stat" -eq 0 ]; then
			kernel=`uname -r 2>&1`;
	else
		kernel="Unable to get Kernel";
	fi
	
#manufacturer=`cat /sys/class/dmi/id/chassis_vendor`;
#Manufacturer
	manufacturer_checker=`cat /sys/class/dmi/id/chassis_vendor  2>&1`;
	manufacturer_checker_stat="$?";
	manudmi_checker=`dmidecode -s baseboard-manufacturer  2>&1`;
	manudmi_checker_stat="$?";
	
	if [ "$manufacturer_checker_stat" -eq 0 ]; then
			manufacturer=`cat /sys/class/dmi/id/chassis_vendor | sed 's/|//' 2>&1`;
		elif [ "$manudmi_checker_stat" -eq 0 ]; then
			manufacturer=`dmidecode -s baseboard-manufacturer | sed 's/|//' 2>&1`;
	else
		manufacturer="Unable to get Manufacturer";
	fi

#InstallDate=`fs=$(df / | tail -1 | cut -f1 -d' ') && tune2fs -l $fs | grep 'Filesystem created:' | sed "s/Filesystem created://" | sed -e 's/^[[:space:]]*//'`;
#Install Date
	basesystem_checker=`rpm -q basesystem 2>&1`;
	basesystem_checker_stat="$?";
	tune2fs_checker=`fs=$(df / | tail -1 | cut -f1 -d' ') && tune2fs -l $fs 2>&1`;
	tune2fs_checker_stat="$?";
	rpmlast_checker=`rpm -qa --last 2>&1`;
	rpmlast_checker_stat="$?";
	
	if [ "$basesystem_checker_stat" -eq 0 ]; then 
			installDate=`rpm -q basesystem --qf '%{installtime:date}\n' | sed 's/|//' 2>&1`;
		elif [ "$tune2fs_checker_stat" -eq 0 ]; then 
			installDate=`fs=$(df / | tail -1 | cut -f1 -d' ') && tune2fs -l $fs | grep 'Filesystem created:' | sed "s/Filesystem created://" | sed -e 's/^[[:space:]]*//' | sed 's/|//' 2>&1`;
		elif [ "$rpmlast_checker_stat" -eq 0 ]; then 
			installDate=`rpm -qa --last | tail -1 | tr -s [:blank:] | cut -d ' ' -f2- | sed 's/|//' 2>&1`;
	else
		installDate="Unable to get Install Date";
	fi
	
#systemBootTime=`who -b | grep -w 'system boot' | sed "s/system boot//" | sed -e 's/^[[:space:]]*//'`;
#System Boot Time
	systemBoot_checker=`who -b | grep -w 'system boot' | sed "s/system boot//" | sed -e 's/^[[:space:]]*//'  2>&1`;
	systemBoot_checker_stat="$?";
	
	if [ "$systemBoot_checker_stat" -eq 0 ]; then
			systemBootTime=`who -b | grep -w 'system boot' | sed "s/system boot//" | sed -e 's/^[[:space:]]*//' | sed 's/|//' 2>&1`;
	else
		systemBootTime="Unable to get System Boot Time";
	fi

#systemManufacturer=`dmidecode -s system-manufacturer`;
#System Manufacturer
	systemManu_checker=`dmidecode -s system-manufacturer 2>&1`;
	systemManu_checker_stat="$?";
	
	if [ "$systemManu_checker_stat" -eq 0 ]; then
			systemManufacturer=`dmidecode -s system-manufacturer | sed 's/|//' 2>&1`;
	else
		systemManufacturer="Unable to get System Manufacturer";
	fi
	
#systemModel=`dmidecode -s system-product-name`;
#System Model
	systemMod_checker=`dmidecode -s system-product-name  2>&1`;
	systemMod_checker_stat="$?";
	
	if [ "$systemMod_checker_stat" -eq 0 ]; then
			systemModel=`dmidecode -s system-product-name | sed 's/|//' 2>&1`;
	else
		systemModel="Unable to get System Model";
	fi

#systemType=`arch`;
#System Type
	systemArch_checker=`arch  2>&1`;
	systemArch_checker_stat="$?";
	
	if [ "$systemArch_checker_stat" -eq 0 ]; then
			systemType=`arch 2>&1`;
	else
		systemType="Unable to get System Type";
	fi

#domain=`domainname`;
#Domain Name	
	d_checker=`domainname 2>&1`;
	d_checker_stat="$?";
	
	if [ "$d_checker_stat" -eq 0 ]; then 
			domain=`domainname 2>&1`;
	else
		Domain="unable to get Domain Name";
	fi

#networkCard=`lspci | egrep -i --color 'network|ethernet' | cut -d ' ' -f2-`;
#Network Card, IP Address, and MAC Address
netlspci_checker=`lspci 2>/dev/null`;
netlspci_checker_stat="$?";

if [ "${netlspci_checker_stat}" -eq 0 ]; then
		netCount=`lspci | egrep -i --color 'network|ethernet' | wc -l 2>/dev/null`;
	else
		netCount="0";
fi

networkCardRes=();
ipAddrRes=();
macaddresRes=();

if [ "$netCount" -ge 1 ]; then

for (( ctr=1; ctr<=$netCount; ctr++ ))
do
    netcard_count=`lspci | egrep -i --color 'network|ethernet' | cut -d ' ' -f2- | awk 'NR=='"$ctr"' {print}' | wc -l 2>&1`;
		if [ $netcard_count -eq 1 ]; then
#Network Card
			ncard=`lspci | egrep -i --color 'network|ethernet' | cut -d ' ' -f4- | awk 'NR=='"$ctr"' {print}' 2>&1`;

#IP Address			
			ipshow_checker=`ip address show | grep "inet " | grep "brd " | grep -v "host" | awk 'NR=='"$ctr"' {print}' 2>&1`;
			ipshow_checker_stat="$?";
			
	
			if [ "$ipshow_checker_stat" -eq 0 ]; then
					ippAddr=`ip address show | grep "inet " | grep "brd " | grep -v "host" | sed "s/inet//" | awk -F"/" '{print $1}' | sed -e 's/^[[:space:]]*//' | awk 'NR=='"$ctr"' {print}' 2>&1`;
			else
					ippAddr="N/A";
			fi
			
#MAC Address
			mcaddr_checker=`ip link show | grep "link/ether " | awk '{print $2}' | awk 'NR=='"$ctr"' {print}' 2>&1`;
			mcaddr_checker_stat="$?";
				
			if [ "$mcaddr_checker_stat" -eq 0 ]; then
					mcaddr=`ip link show | grep "link/ether " | awk '{print $2}' | awk 'NR=='"$ctr"' {print}' 2>&1`;
			else
					mcaddr="N/A";
			fi
			
			nl=$',';		
			
			networkCardRes+="${ncard}${nl}"
			ipAddrRes+="${ippAddr}${nl}"
			macaddressRes+="${mcaddr}${nl}"
			#networkCard+="$(echo "$netCard" | sed 's/,$//')"
		fi;
done
	networkCard=`echo "${networkCardRes[@]}" | sed 's/,$//g'`
	ipAddr=`echo "${ipAddrRes[@]}" | sed 's/,$//g'`
	macaddress=`echo "${macaddressRes[@]}" | sed 's/,$//g'`
	
else
	networkHW_checker=`lshw -class network 2>/dev/null`;
	networkHW_checker_stat="$?";
	
			if [ "${networkHW_checker_stat}" -eq 0 ]; then
					network_prod=`lshw -class network | grep -w 'product:' | sed "s/product://" | sed -e 's/^[[:space:]]*//'`;
					network_vend=`lshw -class network | grep -w 'vendor:' | sed "s/vendor://" | sed -e 's/^[[:space:]]*//'`;
					networkCard="${network_vend} ${network_prod}"
			else		
					networkCard="N/A";
			fi;
	
	ipAddr=`hostname -I | awk '{print$1}'`;
	macaddressbond=`ip address show | grep "${ipAddr}" | awk '{print$7}' 2> /dev/null`;
	mcaddr_checker=`ip link show "${macaddressbond}" | grep "link/ether" | wc -l 2> /dev/null`;

			if [ "${mcaddr_checker}" -eq 1 ]; then
					macaddress=`ip link show "${macaddressbond}" | grep "link/ether " | awk '{print $2}' 2>&1`;
			else
					macaddress="N/A";
			fi;
fi;	
	
	echo "${cDate}|${hostname}|${operatingSystem}|${kernel}|${manufacturer}|${installDate}|${systemBootTime}|${systemManufacturer}|${systemModel}|${systemType}|${domain}|${networkCard[@]}|${ipAddr[@]}|${macaddress[@]}";

}

function suseInfo()
{
cDate=$(date '+%Y-%m-%d %H:%M:%S');
#hostname=`hostname`;
#Hostname
	hostname_checker=`hostname 2>&1`;
	hostname_checker_stat="$?";
	
	if [ "$hostname_checker_stat" -eq 0 ]; then
			hostname=`hostname 2>&1`;
	else
		hostname="Unable to get Hostname";
	fi

#OperatingSystem
	hostnamectl_checker=`hostnamectl 2>&1`;
	hostnamectl_checker_stat="$?";
	lsb_checker=`lsb_release -a 2>&1`; 
	lsb_checker_stat="$?";
	etc_checker=` `;
	etc_checker_stat="$?";

	if [ "$hostnamectl_checker_stat" -eq 0 ]; then 
			operatingSystem=`hostnamectl | grep "Operating System" | cut -d ' ' -f5- | sed 's/|//' 2>&1`;
		elif [ "$lsb_checker_stat" -eq 0 ]; then 
			resOS=`lsb_release -si | sed 's/|//' 2>&1`;
			resVR=`lsb_release -sr | sed 's/|//' 2>&1`;
			operatingSystem="$resOS $resVR";
		elif [ "$etc_checker_stat" -eq 0 ]; then 
			resOS=`cat /etc/*-release | grep -w NAME | sed -e "s/NAME=//" | tr -d \" | sed 's/|//' 2>&1`;
			resVR=`cat /etc/*-release | grep -w VERSION | sed -e "s/VERSION=//" | tr -d \" | sed 's/|//' 2>&1`;
			operatingSystem="$resOS $resVR";

			if [ -z "$resOS" ]; then 
				operatingSystem=`sed -n '1p' /etc/*release | sed 's/|//' 2>&1`;
			fi
			
	else
		operatingSystem="Unable to get Operating System";
	fi
	
#kernel=`uname -r`;
#Kernel	
	kernel_checker=`uname -r  2>&1`;
	kernel_checker_stat="$?";
	
	if [ "$kernel_checker_stat" -eq 0 ]; then
			kernel=`uname -r 2>&1`;
	else
		kernel="Unable to get Kernel";
	fi

#manufacturer=`cat /sys/class/dmi/id/chassis_vendor 2>&1`;
#Manufacturer
	manufacturer_checker=`cat /sys/class/dmi/id/chassis_vendor  2>&1`;
	manufacturer_checker_stat="$?";
	manudmi_checker=`dmidecode -s baseboard-manufacturer  2>&1`;
	manudmi_checker_stat="$?";
	
	if [ "$manufacturer_checker_stat" -eq 0 ]; then
			manufacturer=`cat /sys/class/dmi/id/chassis_vendor | sed 's/|//' 2>&1`;
		elif [ "$manudmi_checker_stat" -eq 0 ]; then
			manufacturer=`dmidecode -s baseboard-manufacturer | sed 's/|//' 2>&1`;
	else
		manufacturer="Unable to get Manufacturer";
	fi	
	
#InstallDate=`fs=$(df / | tail -1 | cut -f1 -d' ') && tune2fs -l $fs | grep 'Filesystem created:' | sed "s/Filesystem created://" | sed -e 's/^[[:space:]]*//'`;
#Install Date	
	basesystem_checker=`rpm -q basesystem 2>&1`;
	basesystem_checker_stat="$?";
	tune2fs_checker=`fs=$(df / | tail -1 | cut -f1 -d' ') && tune2fs -l $fs 2>&1`;
	tune2fs_checker_stat="$?";
	rpmlast_checker=`rpm -qa --last 2>&1`;
	rpmlast_checker_stat="$?";
	
	if [ "$basesystem_checker_stat" -eq 0 ]; then 
			installDate=`rpm -q basesystem --qf '%{installtime:date}\n' | sed 's/|//' 2>&1`;
		elif [ "$tune2fs_checker_stat" -eq 0 ]; then 
			installDate=`fs=$(df / | tail -1 | cut -f1 -d' ') && tune2fs -l $fs | grep 'Filesystem created:' | sed "s/Filesystem created://" | sed -e 's/^[[:space:]]*//' | sed 's/|//' 2>&1`;
		elif [ "$rpmlast_checker_stat" -eq 0 ]; then 
			installDate=`rpm -qa --last | tail -1 | tr -s [:blank:] | cut -d ' ' -f2- | sed 's/|//' 2>&1`;
	else
		installDate="Unable to get Install Date";
	fi

#systemBootTime=`who -b | grep -w 'system boot' | sed "s/system boot//" | sed -e 's/^[[:space:]]*//'`;
#System Boot Time
	systemBoot_checker=`who -b | grep -w 'system boot' | sed "s/system boot//" | sed -e 's/^[[:space:]]*//'  2>&1`;
	systemBoot_checker_stat="$?";
	
	if [ "$systemBoot_checker_stat" -eq 0 ]; then
			systemBootTime=`who -b | grep -w 'system boot' | sed "s/system boot//" | sed -e 's/^[[:space:]]*//' | sed 's/|//' 2>&1`;
	else
		systemBootTime="Unable to get System Boot Time";
	fi
	
#systemManufacturer=`dmidecode -s system-manufacturer`;
#System Manufacturer
	systemManu_checker=`dmidecode -s system-manufacturer  2>&1`;
	systemManu_checker_stat="$?";
	
	if [ "$systemManu_checker_stat" -eq 0 ]; then
			systemManufacturer=`dmidecode -s system-manufacturer | sed 's/|//' 2>&1`;
	else
		systemManufacturer="Unable to get System Manufacturer";
	fi	

#systemModel=`dmidecode -s system-product-name`;
#System Model
	systemMod_checker=`dmidecode -s system-product-name  2>&1`;
	systemMod_checker_stat="$?";
	
	if [ "$systemMod_checker_stat" -eq 0 ]; then
			systemModel=`dmidecode -s system-product-name | sed 's/|//' 2>&1`;
	else
		systemModel="Unable to get System Model";
	fi	
	
#systemType=`arch`;
#System Type
	systemArch_checker=`arch  2>&1`;
	systemArch_checker_stat="$?";
	
	if [ "$systemArch_checker_stat" -eq 0 ]; then
			systemType=`arch 2>&1`;
	else
		systemType="Unable to get System Type";
	fi

#domain	
	domain_checker=`domainname 2>&1`;
	domain_checker_stat="$?";
	hostnameD_checker=`hostname -d 2>&1`;
	hostnameD_checker_stat="$?";
	
	if [ "$domain_checker_stat" -eq 0 ]; then 
			domain=`domainname 2>&1`;
			if [ -z "$domain" ]; then 
				domain="(none)";
			fi
		elif [ "$hostnameD_checker_stat" -eq 0 ]; then 
			domain=`hostname -d 2>&1`;
	else
		domain="Unable to get Domain Name";
	fi
	
#networkCard=`lspci | egrep -i --color 'network|ethernet' | cut -d ' ' -f2-`;
#Network Card, IP Address, and MAC Address
netlspci_checker=`lspci 2>/dev/null`;
netlspci_checker_stat="$?";

if [ "${netlspci_checker_stat}" -eq 0 ]; then
		netCount=`lspci | egrep -i --color 'network|ethernet' | wc -l 2>/dev/null`;
	else
		netCount="0";
fi

networkCardRes=();
ipAddrRes=();
macaddresRes=();

if [ "$netCount" -ge 1 ]; then

for (( ctr=1; ctr<=$netCount; ctr++ ))
do
    netcard_count=`lspci | egrep -i --color 'network|ethernet' | cut -d ' ' -f2- | awk 'NR=='"$ctr"' {print}' | wc -l 2>&1`;
		if [ $netcard_count -eq 1 ]; then
#Network Card
			ncard=`lspci | egrep -i --color 'network|ethernet' | cut -d ' ' -f4- | awk 'NR=='"$ctr"' {print}' 2>&1`;

#IP Address			
			ipshow_checker=`ip address show | grep "inet " | grep "brd " | grep -v "host" | awk 'NR=='"$ctr"' {print}' 2>&1`;
			ipshow_checker_stat="$?";
			ifconfig_checker=`ifconfig | grep "inet addr:" | grep "Bcast:" | awk 'NR=='"$ctr"' {print}' 2>&1`;
			ifconfig_checker_stat="$?";
			ifconfig1_checker=`ifconfig | grep "inet " | grep "broadcast " | sed "s/inet//" | awk '{print $1}' | awk 'NR=='"$ctr"' {print}' 2>&1`;
			ifconfig1_checker_stat="$?";
	
			if [[ "$ipshow_checker_stat" -eq 0  && ! -z "$ipshow_checker" ]]; then
					ippAddr=`ip address show | grep "inet " | grep "brd " | grep -v "host" | sed "s/inet//" | awk -F"/" '{print $1}' | sed -e 's/^[[:space:]]*//' | awk 'NR=='"$ctr"' {print}' 2>&1`;
				elif	[[ "$ifconfig1_checker_stat" -eq 0  && ! -z "$ifconfig1_checker" ]]; then
					ippAddr=`ifconfig | grep "inet " | grep "broadcast " | sed "s/inet//" | awk '{print $1}' | awk 'NR=='"$ctr"' {print}' 2>&1`;
				elif	[[ "$ifconfig_checker_stat" -eq 0  && ! -z "$ifconfig_checker" ]]; then
					ippAddr=`ifconfig | grep "inet addr:" | grep "Bcast:" | sed "s/inet addr://" | awk '{print $1}' | awk 'NR=='"$ctr"' {print}' 2>&1`;
			else
					ippAddr="N/A";
			fi
			
#MAC Address
			mcaddr_checker=`ip link show | grep "link/ether " | awk '{print $2}' | awk 'NR=='"$ctr"' {print}' 2>&1`;
			mcaddr_checker_stat="$?";
			mcaddr2_checker=`ifconfig | grep "ether " | sed "s/ether//" | awk '{print $1}' | awk 'NR=='"$ctr"' {print}' 2>&1`;
			mcaddr2_checker_stat="$?";
			hwAddr_checker=`ifconfig | grep "HWaddr " | sed "s/HWaddr//" | awk '{print $1}' | awk 'NR=='"$ctr"' {print}' 2>&1`;
			hwAddr_checker_stat="$?";	

			if [[ "$mcaddr_checker_stat" -eq 0  && ! -z "$mcaddr_checker" ]]; then
					mcaddr=`ip link show | grep "link/ether " | awk '{print $2}' | awk 'NR=='"$ctr"' {print}' 2>&1`;
				elif [[ "$mcaddr2_checker_stat" -eq 0  && ! -z "$mcaddr2_checker" ]]; then
					mcaddr=`ifconfig | grep "ether " | sed "s/ether//" | awk '{print $1}' | awk 'NR=='"$ctr"' {print}' 2>&1`;
				elif [[ "$hwAddr_checker_stat" -eq 0 && ! -z "$hwAddr_checker" ]]; then
					mcaddr=`ifconfig | grep "HWaddr " | sed "s/HWaddr//" | awk '{print $1}' | awk 'NR=='"$ctr"' {print}' 2>&1`;
			else
					mcaddr="N/A";
			fi
			
			nl=$',';		
			
			networkCardRes+="${ncard}${nl}"
			ipAddrRes+="${ippAddr}${nl}"
			macaddressRes+="${mcaddr}${nl}"
			#networkCard+="$(echo "$netCard" | sed 's/,$//')"
		fi;
done
	networkCard=`echo "${networkCardRes[@]}" | sed 's/,$//g'`
	ipAddr=`echo "${ipAddrRes[@]}" | sed 's/,$//g'`
	macaddress=`echo "${macaddressRes[@]}" | sed 's/,$//g'`

else
	networkHW_checker=`lshw -class network 2>/dev/null`;
	networkHW_checker_stat="$?";
	
			if [ "${networkHW_checker_stat}" -eq 0 ]; then
					network_prod=`lshw -class network | grep -w 'product:' | sed "s/product://" | sed -e 's/^[[:space:]]*//'`;
					network_vend=`lshw -class network | grep -w 'vendor:' | sed "s/vendor://" | sed -e 's/^[[:space:]]*//'`;
					networkCard="${network_vend} ${network_prod}"
			else		
					networkCard="N/A";
			fi;
	
	ipAddr=`hostname -I | awk '{print$1}'`;
	macaddressbond=`ip address show | grep "${ipAddr}" | awk '{print$7}' 2> /dev/null`;
	mcaddr_checker=`ip link show "${macaddressbond}" | grep "link/ether" | wc -l 2> /dev/null`;

			if [ "${mcaddr_checker}" -eq 1 ]; then
					macaddress=`ip link show "${macaddressbond}" | grep "link/ether " | awk '{print $2}' 2>&1`;
			else
					macaddress="N/A";
			fi;
fi;	

	echo "${cDate}|${hostname}|${operatingSystem}|${kernel}|${manufacturer}|${installDate}|${systemBootTime}|${systemManufacturer}|${systemModel}|${systemType}|${domain}|${networkCard[@]}|${ipAddr[@]}|${macaddress[@]}";

}

function oracleInfo()
{
cDate=$(date '+%Y-%m-%d %H:%M:%S');
#hostname=`hostname`;
#Hostname
	hostname_checker=`hostname 2>&1`;
	hostname_checker_stat="$?";
	
	if [ "$hostname_checker_stat" -eq 0 ]; then
			hostname=`hostname 2>&1`;
	else
		hostname="Unable to get Hostname";
	fi
	
#OperatingSystem
	hostnamectl_checker=`hostnamectl 2>&1`;
	hostnamectl_checker_stat="$?";
	lsb_checker=`lsb_release -a 2>&1`; 
	lsb_checker_stat="$?";
	etc_checker=`cat /etc/*-release | grep -w NAME 2>&1`;
	etc_checker_stat="$?";

	if [ "$hostnamectl_checker_stat" -eq 0 ]; then 
			operatingSystem=`hostnamectl | grep "Operating System" | cut -d ' ' -f5- | sed 's/|//' 2>&1`;
		elif [ "$etc_checker_stat" -eq 0 ]; then 
			resOS=`cat /etc/*-release | grep -w NAME | sed -e "s/NAME=//" | tr -d \" | sed 's/|//' 2>&1`;
			resVR=`cat /etc/*-release | grep -w VERSION | sed -e "s/VERSION=//" | tr -d \" | sed 's/|//' 2>&1`;
			operatingSystem="${resOS} ${resVR}";
			operatingSystem=`echo ${operatingSystem} | tr -s [:blank:]`;
		elif [ "$lsb_checker_stat" -eq 0 ]; then 
			resOS=`lsb_release -si | sed 's/|//' 2>&1`;
			resVR=`lsb_release -sr | sed 's/|//' 2>&1`;
			operatingSystem="${resOS} ${resVR}";

			if [ -z "$resOS" ]; then 
				operatingSystem=`sed -n '1p' /etc/*release | sed 's/|//' 2>&1`;
			fi
			
	else
		operatingSystem="Unable to get Operating System";
	fi
	
	kernel=`uname -r 2>&1`;
	
#manufacturer=`cat /sys/class/dmi/id/chassis_vendor 2>&1`;
#Manufacturer
	manufacturer_checker=`cat /sys/class/dmi/id/chassis_vendor  2>&1`;
	manufacturer_checker_stat="$?";
	manudmi_checker=`dmidecode -s baseboard-manufacturer  2>&1`;
	manudmi_checker_stat="$?";
	
	if [ "$manufacturer_checker_stat" -eq 0 ]; then
			manufacturer=`cat /sys/class/dmi/id/chassis_vendor | sed 's/|//' 2>&1`;
		elif [ "$manudmi_checker_stat" -eq 0 ]; then
			manufacturer=`dmidecode -s baseboard-manufacturer | sed 's/|//' 2>&1`;
	else
		manufacturer="Unable to get Manufacturer";
	fi
	
#installDate=`rpm -q basesystem --qf '%{installtime:date}\n'`;
#Install Date	
	basesystem_checker=`rpm -q basesystem 2>&1`;
	basesystem_checker_stat="$?";
	tune2fs_checker=`fs=$(df / | tail -1 | cut -f1 -d' ') && tune2fs -l $fs 2>&1`;
	tune2fs_checker_stat="$?";
	rpmlast_checker=`rpm -qa --last 2>&1`;
	rpmlast_checker_stat="$?";
	
	if [ "$basesystem_checker_stat" -eq 0 ]; then 
			installDate=`rpm -q basesystem --qf '%{installtime:date}\n' | sed 's/|//' 2>&1`;
		elif [ "$tune2fs_checker_stat" -eq 0 ]; then 
			installDate=`fs=$(df / | tail -1 | cut -f1 -d' ') && tune2fs -l $fs | grep 'Filesystem created:' | sed "s/Filesystem created://" | sed -e 's/^[[:space:]]*//' | sed 's/|//' 2>&1`;
		elif [ "$rpmlast_checker_stat" -eq 0 ]; then 
			installDate=`rpm -qa --last | tail -1 | tr -s [:blank:] | cut -d ' ' -f2- | sed 's/|//' 2>&1`;
	else
		installDate="Unable to get Install Date";
	fi

#systemBootTime=`who -b | grep -w 'system boot' | sed "s/system boot//" | sed -e 's/^[[:space:]]*//'`;
#System Boot Time
	systemBoot_checker=`who -b | grep -w 'system boot' | sed "s/system boot//" | sed -e 's/^[[:space:]]*//'  2>&1`;
	systemBoot_checker_stat="$?";
	
	if [ "$systemBoot_checker_stat" -eq 0 ]; then
			systemBootTime=`who -b | grep -w 'system boot' | sed "s/system boot//" | sed -e 's/^[[:space:]]*//' | sed 's/|//' 2>&1`;
	else
		systemBootTime="Unable to get System Boot Time";
	fi
	
#systemManufacturer=`dmidecode -s system-manufacturer`;
#System Manufacturer
	systemManu_checker=`dmidecode -s system-manufacturer  2>&1`;
	systemManu_checker_stat="$?";
	
	if [ "$systemManu_checker_stat" -eq 0 ]; then
			systemManufacturer=`dmidecode -s system-manufacturer | sed 's/|//' 2>&1`;
	else
		systemManufacturer="Unable to get System Manufacturer";
	fi
	
#systemModel=`dmidecode -s system-product-name`;
#System Model
	systemMod_checker=`dmidecode -s system-product-name  2>&1`;
	systemMod_checker_stat="$?";
	
	if [ "$systemMod_checker_stat" -eq 0 ]; then
			systemModel=`dmidecode -s system-product-name | sed 's/|//' 2>&1`;
	else
		systemModel="Unable to get System Model";
	fi
	
#systemType=`arch`;
#System Type
	systemArch_checker=`arch  2>&1`;
	systemArch_checker_stat="$?";
	
	if [ "$systemArch_checker_stat" -eq 0 ]; then
			systemType=`arch 2>&1`;
	else
		systemType="Unable to get System Type";
	fi
	
#domain=`domainname`;
#Domain Name
	d_checker=`domainname 2>&1`;
	d_checker_stat="$?";
	
	if [ "$d_checker_stat" -eq 0 ]; then 
			domain=`domainname 2>&1`;
	else
		Domain="unable to get Domain Name";
	fi
	
#NetworkCard=`lspci | egrep -i --color 'network|ethernet' | cut -d ' ' -f2- 2>&1`;
#Network Card, IP Address, and MAC Address
netlspci_checker=`lspci 2>/dev/null`;
netlspci_checker_stat="$?";

if [ "${netlspci_checker_stat}" -eq 0 ]; then
		netCount=`lspci | egrep -i --color 'network|ethernet' | wc -l 2>/dev/null`;
	else
		netCount="0";
fi

networkCardRes=();
ipAddrRes=();
macaddresRes=();

if [ "$netCount" -ge 1 ]; then

for (( ctr=1; ctr<=$netCount; ctr++ ))
do
    netcard_count=`lspci | egrep -i --color 'network|ethernet' | cut -d ' ' -f2- | awk 'NR=='"$ctr"' {print}' | wc -l 2>&1`;
		if [ $netcard_count -eq 1 ]; then
#Network Card
			ncard=`lspci | egrep -i --color 'network|ethernet' | cut -d ' ' -f4- | awk 'NR=='"$ctr"' {print}' 2>&1`;
		
#IP Address			
			ipshow_checker=`ip address show | grep "inet " | grep "brd " | grep -v "host" | awk 'NR=='"$ctr"' {print}' 2>&1`;
			ipshow_checker_stat="$?";
			ifconfig_checker=`ifconfig | grep "inet addr:" | grep "Bcast:" | awk 'NR=='"$ctr"' {print}' 2>&1`;
			ifconfig_checker_stat="$?";
			ifconfig1_checker=`ifconfig | grep "inet " | grep "broadcast " | sed "s/inet//" | awk '{print $1}' | awk 'NR=='"$ctr"' {print}' 2>&1`;
			ifconfig1_checker_stat="$?";
	
			if [[ "$ipshow_checker_stat" -eq 0  && ! -z "$ipshow_checker" ]]; then
					ippAddr=`ip address show | grep "inet " | grep "brd " | grep -v "host" | sed "s/inet//" | awk -F"/" '{print $1}' | sed -e 's/^[[:space:]]*//' | awk 'NR=='"$ctr"' {print}' 2>&1`;
				elif	[[ "$ifconfig1_checker_stat" -eq 0  && ! -z "$ifconfig1_checker" ]]; then
					ippAddr=`ifconfig | grep "inet " | grep "broadcast " | sed "s/inet//" | awk '{print $1}' | awk 'NR=='"$ctr"' {print}' 2>&1`;
				elif	[[ "$ifconfig_checker_stat" -eq 0  && ! -z "$ifconfig_checker" ]]; then
					ippAddr=`ifconfig | grep "inet addr:" | grep "Bcast:" | sed "s/inet addr://" | awk '{print $1}' | awk 'NR=='"$ctr"' {print}' 2>&1`;
				elif [ $netCount -eq 0 ]; then
					ippAddr="N/A";
			
			else
					ippAddr="N/A";
			fi
			
#MAC Address
			mcaddr_checker=`ip link show | grep "link/ether " | awk '{print $2}' | awk 'NR=='"$ctr"' {print}' 2>&1`;
			mcaddr_checker_stat="$?";
			mcaddr2_checker=`ifconfig | grep "ether " | sed "s/ether//" | awk '{print $1}' | awk 'NR=='"$ctr"' {print}' 2>&1`;
			mcaddr2_checker_stat="$?";
			hwAddr_checker=`ifconfig | grep "HWaddr " | sed "s/HWaddr//" | awk '{print $1}' | awk 'NR=='"$ctr"' {print}' 2>&1`;
			hwAddr_checker_stat="$?";	

			if [[ "$mcaddr_checker_stat" -eq 0  && ! -z "$mcaddr_checker" ]]; then
					mcaddr=`ip link show | grep "link/ether " | awk '{print $2}' | awk 'NR=='"$ctr"' {print}' 2>&1`;
				elif [[ "$mcaddr2_checker_stat" -eq 0  && ! -z "$mcaddr2_checker" ]]; then
					mcaddr=`ifconfig | grep "ether " | sed "s/ether//" | awk '{print $1}' | awk 'NR=='"$ctr"' {print}' 2>&1`;
				elif [[ "$hwAddr_checker_stat" -eq 0 && ! -z "$hwAddr_checker" ]]; then
					mcaddr=`ifconfig | grep "HWaddr " | sed "s/HWaddr//" | awk '{print $1}' | awk 'NR=='"$ctr"' {print}' 2>&1`;
			else
					mcaddr="N/A";
			fi;
			
			nl=$',';		
			
			networkCardRes+="${ncard}${nl}"
			ipAddrRes+="${ippAddr}${nl}"
			macaddressRes+="${mcaddr}${nl}"
			#networkCard+="$(echo "$netCard" | sed 's/,$//')"
		fi;
done

	networkCard=`echo "${networkCardRes[@]}" | sed 's/,$//g'`
	ipAddr=`echo "${ipAddrRes[@]}" | sed 's/,$//g'`
	macaddress=`echo "${macaddressRes[@]}" | sed 's/,$//g'`

else
	networkHW_checker=`lshw -class network 2>/dev/null`;
	networkHW_checker_stat="$?";
	
			if [ "${networkHW_checker_stat}" -eq 0 ]; then
					network_prod=`lshw -class network | grep -w 'product:' | sed "s/product://" | sed -e 's/^[[:space:]]*//'`;
					network_vend=`lshw -class network | grep -w 'vendor:' | sed "s/vendor://" | sed -e 's/^[[:space:]]*//'`;
					networkCard="${network_vend} ${network_prod}"
			else		
					networkCard="N/A";
			fi;
	
	ipAddr=`hostname -i | awk '{print$1}'`;
	macaddressbond=`ip address show | grep "${ipAddr}" | awk '{print$7}' 2> /dev/null`;
	mcaddr_checker=`ip link show "${macaddressbond}" | grep "link/ether" | wc -l 2> /dev/null`;

			if [ "${mcaddr_checker}" -eq 1 ]; then
					macaddress=`ip link show "${macaddressbond}" | grep "link/ether " | awk '{print $2}' 2>&1`;
			else
					macaddress="N/A";
			fi;
fi;	
	echo "${cDate}|${hostname}|${operatingSystem}|${kernel}|${manufacturer}|${installDate}|${systemBootTime}|${systemManufacturer}|${systemModel}|${systemType}|${domain}|${networkCard[@]}|${ipAddr[@]}|${macaddress[@]}";

}

function eulerInfo()
{
cDate=$(date '+%Y-%m-%d %H:%M:%S');
#hostname=`hostname`;
#Hostname
	hostname_checker=`hostname 2>&1`;
	hostname_checker_stat="$?";
	
	if [ "$hostname_checker_stat" -eq 0 ]; then
			hostname=`hostname 2>&1`;
	else
		hostname="Unable to get Hostname";
	fi
	
#OperatingSystem
	hostnamectl_checker=`hostnamectl 2>&1`;
	hostnamectl_checker_stat="$?";
	lsb_checker=`lsb_release -a 2>&1`; 
	lsb_checker_stat="$?";
	etc_checker=` `;
	etc_checker_stat="$?";

	if [ "$hostnamectl_checker_stat" -eq 0 ]; then 
			operatingSystem=`hostnamectl | grep "Operating System" | cut -d ' ' -f5- | sed 's/|//' 2>&1`;
		elif [ "$lsb_checker_stat" -eq 0 ]; then 
			resOS=`lsb_release -si | sed 's/|//' 2>&1`;
			resVR=`lsb_release -sr | sed 's/|//' 2>&1`;
			operatingSystem="$resOS $resVR";
		elif [ "$etc_checker_stat" -eq 0 ]; then 
			resOS=`cat /etc/*-release | grep -w NAME | sed -e "s/NAME=//" | tr -d \" | sed 's/|//' 2>&1`;
			resVR=`cat /etc/*-release | grep -w VERSION | sed -e "s/VERSION=//" | tr -d \" | sed 's/|//' 2>&1`;
			operatingSystem="$resOS $resVR";

			if [ -z "$resOS" ]; then 
				operatingSystem=`sed -n '1p' /etc/*release | sed 's/|//' 2>&1`;
			fi
			
	else
		operatingSystem="Unable to get Operating System";
	fi
	
	kernel=`uname -r 2>&1`;
	
#manufacturer=`cat /sys/class/dmi/id/chassis_vendor 2>&1`;
#Manufacturer
	manufacturer_checker=`cat /sys/class/dmi/id/chassis_vendor  2>&1`;
	manufacturer_checker_stat="$?";
	manudmi_checker=`dmidecode -s baseboard-manufacturer  2>&1`;
	manudmi_checker_stat="$?";
	
	if [ "$manufacturer_checker_stat" -eq 0 ]; then
			manufacturer=`cat /sys/class/dmi/id/chassis_vendor | sed 's/|//' 2>&1`;
		elif [ "$manudmi_checker_stat" -eq 0 ]; then
			manufacturer=`dmidecode -s baseboard-manufacturer | sed 's/|//' 2>&1`;
	else
		manufacturer="Unable to get Manufacturer";
	fi
	
#installDate=`rpm -q basesystem --qf '%{installtime:date}\n'`;
#Install Date	
	basesystem_checker=`rpm -q basesystem 2>&1`;
	basesystem_checker_stat="$?";
	tune2fs_checker=`fs=$(df / | tail -1 | cut -f1 -d' ') && tune2fs -l $fs 2>&1`;
	tune2fs_checker_stat="$?";
	rpmlast_checker=`rpm -qa --last 2>&1`;
	rpmlast_checker_stat="$?";
	
	if [ "$basesystem_checker_stat" -eq 0 ]; then 
			installDate=`rpm -q basesystem --qf '%{installtime:date}\n' | sed 's/|//' 2>&1`;
		elif [ "$tune2fs_checker_stat" -eq 0 ]; then 
			installDate=`fs=$(df / | tail -1 | cut -f1 -d' ') && tune2fs -l $fs | grep 'Filesystem created:' | sed "s/Filesystem created://" | sed -e 's/^[[:space:]]*//' | sed 's/|//' 2>&1`;
		elif [ "$rpmlast_checker_stat" -eq 0 ]; then 
			installDate=`rpm -qa --last | tail -1 | tr -s [:blank:] | cut -d ' ' -f2- | sed 's/|//' 2>&1`;
	else
		installDate="Unable to get Install Date";
	fi

#systemBootTime=`who -b | grep -w 'system boot' | sed "s/system boot//" | sed -e 's/^[[:space:]]*//'`;
#System Boot Time
	systemBoot_checker=`who -b | grep -w 'system boot' | sed "s/system boot//" | sed -e 's/^[[:space:]]*//'  2>&1`;
	systemBoot_checker_stat="$?";
	
	if [ "$systemBoot_checker_stat" -eq 0 ]; then
			systemBootTime=`who -b | grep -w 'system boot' | sed "s/system boot//" | sed -e 's/^[[:space:]]*//' | sed 's/|//' 2>&1`;
	else
		systemBootTime="Unable to get System Boot Time";
	fi
	
#systemManufacturer=`dmidecode -s system-manufacturer`;
#System Manufacturer
	systemManu_checker=`dmidecode -s system-manufacturer  2>&1`;
	systemManu_checker_stat="$?";
	
	if [ "$systemManu_checker_stat" -eq 0 ]; then
			systemManufacturer=`dmidecode -s system-manufacturer | sed 's/|//' 2>&1`;
	else
		systemManufacturer="Unable to get System Manufacturer";
	fi
	
#systemModel=`dmidecode -s system-product-name`;
#System Model
	systemMod_checker=`dmidecode -s system-product-name  2>&1`;
	systemMod_checker_stat="$?";
	
	if [ "$systemMod_checker_stat" -eq 0 ]; then
			systemModel=`dmidecode -s system-product-name | sed 's/|//' 2>&1`;
	else
		systemModel="Unable to get System Model";
	fi
	
#systemType=`arch`;
#System Type
	systemArch_checker=`arch  2>&1`;
	systemArch_checker_stat="$?";
	
	if [ "$systemArch_checker_stat" -eq 0 ]; then
			systemType=`arch 2>&1`;
	else
		systemType="Unable to get System Type";
	fi
	
#domain=`domainname`;
#Domain Name
	d_checker=`domainname 2>&1`;
	d_checker_stat="$?";
	
	if [ "$d_checker_stat" -eq 0 ]; then 
			domain=`domainname 2>&1`;
	else
		Domain="unable to get Domain Name";
	fi
	
#NetworkCard=`lspci | egrep -i --color 'network|ethernet' | cut -d ' ' -f2- 2>&1`;
#Network Card, IP Address, and MAC Address
netlspci_checker=`lspci 2>/dev/null`;
netlspci_checker_stat="$?";

if [ "${netlspci_checker_stat}" -eq 0 ]; then
		netCount=`lspci | egrep -i --color 'network|ethernet' | wc -l 2>/dev/null`;
	else
		netCount="0";
fi

networkCardRes=();
ipAddrRes=();
macaddresRes=();

if [ "$netCount" -ge 1 ]; then

for (( ctr=1; ctr<=$netCount; ctr++ ))
do
    netcard_count=`lspci | egrep -i --color 'network|ethernet' | cut -d ' ' -f2- | awk 'NR=='"$ctr"' {print}' | wc -l 2>&1`;
		if [ $netcard_count -eq 1 ]; then
#Network Card
			ncard=`lspci | egrep -i --color 'network|ethernet' | cut -d ' ' -f4- | awk 'NR=='"$ctr"' {print}' 2>&1`;

#IP Address			
			ipshow_checker=`ip address show | grep "inet " | grep "brd " | grep -v "host" | awk 'NR=='"$ctr"' {print}' 2>&1`;
			ipshow_checker_stat="$?";
			ifconfig_checker=`ifconfig | grep "inet addr:" | grep "Bcast:" | awk 'NR=='"$ctr"' {print}' 2>&1`;
			ifconfig_checker_stat="$?";
			ifconfig1_checker=`ifconfig | grep "inet " | grep "broadcast " | sed "s/inet//" | awk '{print $1}' | awk 'NR=='"$ctr"' {print}' 2>&1`;
			ifconfig1_checker_stat="$?";
	
			if [[ "$ipshow_checker_stat" -eq 0  && ! -z "$ipshow_checker" ]]; then
					ippAddr=`ip address show | grep "inet " | grep "brd " | grep -v "host" | sed "s/inet//" | awk -F"/" '{print $1}' | sed -e 's/^[[:space:]]*//' | awk 'NR=='"$ctr"' {print}' 2>&1`;
				elif	[[ "$ifconfig1_checker_stat" -eq 0  && ! -z "$ifconfig1_checker" ]]; then
					ippAddr=`ifconfig | grep "inet " | grep "broadcast " | sed "s/inet//" | awk '{print $1}' | awk 'NR=='"$ctr"' {print}' 2>&1`;
				elif	[[ "$ifconfig_checker_stat" -eq 0  && ! -z "$ifconfig_checker" ]]; then
					ippAddr=`ifconfig | grep "inet addr:" | grep "Bcast:" | sed "s/inet addr://" | awk '{print $1}' | awk 'NR=='"$ctr"' {print}' 2>&1`;
			else
					ippAddr="N/A";
			fi
			
#MAC Address
			mcaddr_checker=`ip link show | grep "link/ether " | awk '{print $2}' | awk 'NR=='"$ctr"' {print}' 2>&1`;
			mcaddr_checker_stat="$?";
			mcaddr2_checker=`ifconfig | grep "ether " | sed "s/ether//" | awk '{print $1}' | awk 'NR=='"$ctr"' {print}' 2>&1`;
			mcaddr2_checker_stat="$?";
			hwAddr_checker=`ifconfig | grep "HWaddr " | sed "s/HWaddr//" | awk '{print $1}' | awk 'NR=='"$ctr"' {print}' 2>&1`;
			hwAddr_checker_stat="$?";	

			if [[ "$mcaddr_checker_stat" -eq 0  && ! -z "$mcaddr_checker" ]]; then
					mcaddr=`ip link show | grep "link/ether " | awk '{print $2}' | awk 'NR=='"$ctr"' {print}' 2>&1`;
				elif [[ "$mcaddr2_checker_stat" -eq 0  && ! -z "$mcaddr2_checker" ]]; then
					mcaddr=`ifconfig | grep "ether " | sed "s/ether//" | awk '{print $1}' | awk 'NR=='"$ctr"' {print}' 2>&1`;
				elif [[ "$hwAddr_checker_stat" -eq 0 && ! -z "$hwAddr_checker" ]]; then
					mcaddr=`ifconfig | grep "HWaddr " | sed "s/HWaddr//" | awk '{print $1}' | awk 'NR=='"$ctr"' {print}' 2>&1`;
			else
					mcaddr="N/A";
			fi
			
			nl=$',';		
			
			networkCardRes+="${ncard}${nl}"
			ipAddrRes+="${ippAddr}${nl}"
			macaddressRes+="${mcaddr}${nl}"
			#networkCard+="$(echo "$netCard" | sed 's/,$//')"
		fi;
done
	networkCard=`echo "${networkCardRes[@]}" | sed 's/,$//g'`
	ipAddr=`echo "${ipAddrRes[@]}" | sed 's/,$//g'`
	macaddress=`echo "${macaddressRes[@]}" | sed 's/,$//g'`

else
	networkHW_checker=`lshw -class network 2>/dev/null`;
	networkHW_checker_stat="$?";
	
			if [ "${networkHW_checker_stat}" -eq 0 ]; then
					network_prod=`lshw -class network | grep -w 'product:' | sed "s/product://" | sed -e 's/^[[:space:]]*//'`;
					network_vend=`lshw -class network | grep -w 'vendor:' | sed "s/vendor://" | sed -e 's/^[[:space:]]*//'`;
					networkCard="${network_vend} ${network_prod}"
			else		
					networkCard="N/A";
			fi;
	
	ipAddr=`hostname -I | awk '{print$1}'`;
	macaddressbond=`ip address show | grep "${ipAddr}" | awk '{print$7}' 2> /dev/null`;
	mcaddr_checker=`ip link show "${macaddressbond}" | grep "link/ether" | wc -l 2> /dev/null`;

			if [ "${mcaddr_checker}" -eq 1 ]; then
					macaddress=`ip link show "${macaddressbond}" | grep "link/ether " | awk '{print $2}' 2>&1`;
			else
					macaddress="N/A";
			fi;
fi;	

	echo "${cDate}|${hostname}|${operatingSystem}|${kernel}|${manufacturer}|${installDate}|${systemBootTime}|${systemManufacturer}|${systemModel}|${systemType}|${domain}|${networkCard[@]}|${ipAddr[@]}|${macaddress[@]}";

}

if [[ "${arrOS}" == "${os[1]}" ]]; then
#	echo "CENTOS LOGIC HERE";
	centosInfo
elif [[ "${arrOS}" == "${os[2]}" ]]; then
#	echo "UBUNTU LOGIC HERE";
	ubuntuInfo
elif [[ "${arrOS}" == "${os[3]}" ]]; then
#	echo "RED HAT LOGIC HERE";
	rhelInfo
elif [[ "${arrOS}" == "${os[4]}" ]]; then
#	echo "SUSE LOGIC HERE";
	suseInfo
elif [[ "${arrOS}" == "${os[5]}" ]]; then
#	echo "ORACLESERVER LOGIC HERE";
	oracleInfo
elif [[ "${arrOS}" == "${os[6]}" ]]; then
#	echo "EULEROS LOGIC HERE";
	eulerInfo
elif [[ "${arrOS}" == "${os[7]}" ]]; then
#	echo "AMAZON LOGIC HERE";
	rhelInfo
elif [[ "${arrOS}" == "${os[8]}" ]]; then
#	echo "ORACLE LINUX SERVER LOGIC HERE";
	oracleInfo
fi

#END=$(date +%s)
#DIFF=$(( $END - $START ))
#echo "It took $DIFF seconds"

