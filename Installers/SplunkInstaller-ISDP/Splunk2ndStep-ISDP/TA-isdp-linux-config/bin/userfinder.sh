#!/bin/bash
#######################################
### CREATED BY: R2C		    ###
### DATE CREATED: 2019-05-30	    ###
### MODIFIED BY: R2C		    ###
### DATE MODIFIED: 2019-05-30	    ###
###				    ###
### SCRIPT TO CHECK THE USERS       ### 
### IN HOME DIRECTORY               ###
#######################################

#######################################
#####   DO NOT TOUCH THIS PART   ######
#######################################
function main () {
	#sleep 60s
	path_val="/home/"
	homedirs=`ls -l $path_val | egrep '^d' | awk '{print $9}'`
	scanResults=''
	for homedir in $homedirs
	do
		sshpathdir="$path_val${homedir}/"
		if [ -d "$sshpathdir" ]; then
			dt=$(date '+%D %H:%M:%S');
			scanResults+="$dt $sshpathdir \n"	
		else
			dt=$(date '+%D %H:%M:%S');
			scanResults+="$dt $sshpathdir Folder not exist. \n"
		fi
	done
	printf "$scanResults"
}

my_filename=$(basename -- "$0")

#CHECK IF THE SCRIPT STILL RUNNING THEN KILL IT
checkIfRunning=`ps -eo lstart,pid,cmd --sort=start_time | grep -m 2 "$my_filename" | grep -v "grep"  | awk '{ cmd="date -d\""$1 FS $2 FS $3 FS $4 FS $5"\" +\047%s\047"; cmd | getline d; close(clearcmd); $1=$2=$3=$4=$5=""; printf "%s\n",d$0 }'`

arrayRun=($checkIfRunning)
datetime=${arrayRun[0]}
procid=${arrayRun[1]}
exetype=${arrayRun[2]}
flname=${arrayRun[3]}
cdatetime=$(date '+%s')
datetimeplusone=$(($datetime + 1))

if [ "$datetimeplusone" -lt "$cdatetime" ]; then
	killMe=$(kill -9 "$procid" 2>/dev/null)
	killMeStatus=$?
	if [ "$killMeStatus" == "0" ]; then
		dt=$(date '+%Y-%m-%d %H:%M:%S');
		#echo "$dt $my_filename has been successfully stopped and Start."
		main # CALL MAIN FUNCTION #
	else
		dt=$(date '+%Y-%m-%d %H:%M:%S');
		echo "$dt $my_filename unable to stop. Error: $killMeStatus"
	fi
else
	main # CALL MAIN FUNCTION #
fi

