<!-- : Begin batch script
@echo off
@SETLOCAL enableextensions enabledelayedexpansion
:::::::::::::::::::::::::::::::::::::::
::: CREATED BY: ISDP-SOC DEV        :::
::: DATE CREATED: 2020-10-05        :::
::: MODIFIED BY: ISDP-SOC DEV       :::
::: DATE MODIFIED: 2020-10-28       :::
:::::::::::::::::::::::::::::::::::::::

::: RUN THE EMBEDED VBScript
cscript //nologo "%~f0?.wsf" %1
exit /b
----- Begin wsf script --->
<job><script language="VBScript">	

	secAgentSource = "SecAgentList.txt"

	'''''''''''''''''''''''''''''''''''''''
	'''     DO NOT TOUCH THIS PART      '''
	'''''''''''''''''''''''''''''''''''''''
	
	Dim appStatResRaw
	Dim strNextLine
	Dim arrSecAgentList
	Dim arrSecAgent
	Dim secAgentName
	Dim secAgentStatRaw
	Dim secAgentProcRaw
	Dim appNameRes
	Dim appStatRes
	Dim appVersDateRes
	Dim appMemCpuRes
	Dim currentDate

	Function timeStamp()
		Dim t 
		t = Now
		timeStamp = Year(t) & "-" & _
		Right("0" & Month(t),2)  & "-" & _
		Right("0" & Day(t),2)  & " " & _  
		Right("0" & Hour(t),2) & ":" & _
		Right("0" & Minute(t),2) & ":" & _    
		Right("0" & Second(t),2) 
	End Function

	Function ConvertSize(Size) 
		Do While InStr(Size,",") 'Remove commas from size     
			CommaLocate = InStr(Size,",")     
			Size = Mid(Size,1,CommaLocate - 1) & _         
			Mid(Size,CommaLocate + 1,Len(Size) - CommaLocate) 
		Loop
		Dim Suffix:Suffix = "Bytes " 
		If Size >= 1024 Then suffix = "KB " 
		If Size >= 1048576 Then suffix = "MB " 
		If Size >= 1073741824 Then suffix = "GB " 
		If Size >= 1099511627776 Then suffix = "TB " 
		Select Case Suffix    
		Case "KB " Size = Round(Size / 1024, 1)     
		Case "MB " Size = Round(Size / 1048576, 1)     
		Case "GB " Size = Round(Size / 1073741824, 1)     
		Case "TB " Size = Round(Size / 1099511627776, 1) 
		End Select
		ConvertSize = Size & Suffix 
	End Function

	Function ConvertDate(instDate)
		dateWthDash = Join(Array(Mid(instDate, 1, 4), Mid(instDate, 5, 2), Mid(instDate, 7, 2)), "-")
		dateSplit = Split(dateWthDash,"-")
		result = MonthName(dateSplit(1),True)+" "+dateSplit(2)+", "+dateSplit(0)
		ConvertDate = result
	End Function
	
	Function secAppStat(secAppStatToSearch)	
		Set objWMIService = GetObject("winmgmts:{impersonationLevel=impersonate}!\\.\root\cimv2")
		Set colListOfServices = objWMIService.ExecQuery ("Select * from Win32_Service WHERE Name ='" & secAppStatToSearch & "'")

		For Each objService in colListOfServices
			secAppStatRaw = objService.State
			If secAppStatRaw = "" Then
				secAppStat = "Not Installed"
			Else
				secAppStatRaw = UCase(Left(secAppStatRaw, 1)) &  Mid(secAppStatRaw, 2)
				secAppStat = Trim(secAppStatRaw)
			End If
		Next
	End Function

	Function secAppVersion(secAppVerToSearch)	
		Const HKLM = &H80000002 'HKEY_LOCAL_MACHINE 
		Dim strComputer, strKey, strKey2, secAppVersionRes
		strComputer = "." 
		strKey = "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\" 
		strKey2 = "SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\" 
		
		Dim objReg, objReg2, strSubkey, arrSubkeys, strSubkey2, arrSubkeys2
		
		Set objReg = GetObject("winmgmts://" & strComputer & "/root/default:StdRegProv") 
		
		objReg.EnumKey HKLM, strKey, arrSubkeys
		
		For Each strSubkey In arrSubkeys 
			objReg.GetStringValue HKLM, strKey & strSubkey, "DisplayName" , appName
        
			If appName = secAppVerToSearch Then
				objReg.GetStringValue HKLM, strKey & strSubkey, "DisplayVersion" , appVersion
				objReg.GetStringValue HKLM, strKey & strSubkey, "InstallDate", appInstallDate
				secAppVersionRes = appVersion & "|" & ConvertDate(appInstallDate) 
			End If 
		Next 
		
		objReg.EnumKey HKLM, strKey2, arrSubkeys2  
		
		For Each strSubkey2 In arrSubkeys2 
			objReg.GetStringValue HKLM, strKey2 & strSubkey2, "DisplayName" , appName2

			If appName2 = secAppVerToSearch Then
				objReg.GetStringValue HKLM, strKey2 & strSubkey2, "DisplayVersion" , appVersion2
				objReg.GetStringValue HKLM, strKey2 & strSubkey2, "InstallDate", appInstallDate2
				secAppVersionRes = appVersion2 & "|" & ConvertDate(appInstallDate2) 
			End If 
		Next 
		
		secAppVersion = secAppVersionRes
		
		
	End Function

	Function secAppCpuUsage(secAppCpuToSearch)	
		Set objWMICpu = GetObject("winmgmts:\\.\root\cimv2")
		Set colObjectsCpu = objWMICpu.ExecQuery("SELECT * FROM Win32_PerfFormattedData_PerfProc_Process WHERE name='" & secAppCpuToSearch & "'")

		For Each ItemCpu in colObjectsCpu
			secAppCpuUsage = ItemCpu.PercentProcessorTime & "%"
		Next
	End Function

	Function secAppMemUsage(secAppMemToSearch)	
		secAppMemToSearch = secAppMemToSearch+".exe"
		Set objWMIMem = GetObject("winmgmts:\\.\root\cimv2")
		Set colObjectsMem = objWMIMem.ExecQuery("SELECT * FROM Win32_Process WHERE name='" & secAppMemToSearch & "'")
		For Each ItemMem in colObjectsMem
			secAppMemUsage = secAppMemUsage + ConvertSize(ItemMem.WorkingSetSize)
		Next
	End Function

	currentDate = timeStamp
	
	Set objFSO = CreateObject("Scripting.FileSystemObject")
	Set objTextFile = objFSO.OpenTextFile(secAgentSource, "1")

	Do Until objTextFile.AtEndOfStream
		strNextLine = objTextFile.Readline
		arrSecAgentList = Split(strNextLine , "")
		arrSecAgent = Split(arrSecAgentList(i),"|")
		secAgentName = Trim(arrSecAgent(0))
		secAgentStatRaw = Trim(arrSecAgent(1))
		secAgentProcRaw = Trim(arrSecAgent(2))
		appNameRes = secAgentName
		appStatResRaw = secAppStat(secAgentStatRaw)
		
		If appStatResRaw = "" Then
			appStatRes = "Not Installed"
			appVersDateRes = "N/A|N/A"
			appMemCpuRes = "N/A|N/A" 
		Else
			appStatRes = appStatResRaw
			appVersDateRes = secAppVersion(appNameRes)
			appMemCpuRes = "CPU - "+secAppCpuUsage(secAgentProcRaw)+" MEM - "+secAppMemUsage(secAgentProcRaw)
		End If
		
		WScript.Echo currentDate + "|" + appNameRes + "|" + appStatRes + "|" + appVersDateRes + "|" + appMemCpuRes
	Loop
</script></job>
