<!-- : Begin batch script
@echo off
@SETLOCAL enableextensions enabledelayedexpansion
:::::::::::::::::::::::::::::::::::::::
::: CREATED BY: ISDP-SOC DEV        :::
::: DATE CREATED: 2020-10-05        :::
::: MODIFIED BY: ISDP-SOC DEV       :::
::: DATE MODIFIED: 2021-02-16       :::
:::::::::::::::::::::::::::::::::::::::

::: RUN THE EMBEDED VBScript
cscript //nologo "%~f0?.wsf" %1
exit /b
----- Begin wsf script --->
<job><script language="VBScript">	


Function timeStamp()
    Dim t 
    t = Now
    timeStamp = Year(t) & "-" & _
    Right("0" & Month(t),2)  & "-" & _
    Right("0" & Day(t),2)  & " " & _  
    Right("0" & Hour(t),2) & ":" & _
    Right("0" & Minute(t),2) & ":" & _    
	Right("0" & Second(t),2) 
End Function

Function WMIDateStringToDate(dtmInstallDate)
    WMIDateStringToDate = CDate(Mid(dtmInstallDate, 5, 2) & "/" & _
        Mid(dtmInstallDate, 7, 2) & "/" & Left(dtmInstallDate, 4) _
            & " " & Mid (dtmInstallDate, 9, 2) & ":" & _
                Mid(dtmInstallDate, 11, 2) & ":" & Mid(dtmInstallDate, _
                    13, 2))
End Function

strComputer = "."
Set objWMIService = GetObject("winmgmts:" _
    & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")
	
	currentDate = timeStamp
	
	
	
	Set colSettings = objWMIService.ExecQuery _
    ("Select * from Win32_OperatingSystem")

For Each objOperatingSystem in colSettings 
   
		

		OSNAME = Replace(objOperatingSystem.Name,"|"," ")
		OSVERSION = Replace(objOperatingSystem.Version,"|"," ")
		OSMANU = Replace(objOperatingSystem.Manufacturer,"|"," ")
		OSCAPTION = Replace(objOperatingSystem.Caption,"|"," ")
		OSIDATE = Replace(WMIDateStringToDate(objOperatingSystem.InstallDate),"|"," ")
		OSLBOOTTIME = Replace(WMIDateStringToDate(objOperatingSystem.LastBootUpTime),"|"," ")
		strOSIDATE = Replace(FormatDateTime(OSIDATE,0),"|"," ")
		strOSLBOOTTIME = Replace(FormatDateTime(OSLBOOTTIME,0),"|"," ")
		
		On Error Resume Next
		OSARC = objOperatingSystem.OSArchitecture
			

				If Err.Number <> 0 Then
					OSARC = ""
				Else
					'no error
				End If
		

		
Next
	
	Set colSettings = objWMIService.ExecQuery _
    ("Select * from Win32_ComputerSystem")

For Each objComputer in colSettings 
   	
		SYSNAME = Replace(objComputer.Name,"|"," ")
		SYSMANU = Replace(objComputer.Manufacturer,"|"," ")
		SYSMOD = Replace(objComputer.Model,"|"," ")
		SYSSTYPE = Replace(objComputer.SystemType,"|"," ")
		SYSDOMAIN = Replace(objComputer.Domain,"|"," ")
		
		SYSSYSTYPE = Replace(objComputer.SystemType,"|"," ")
Next


	

Set colItems = objWMIService.ExecQuery("Select * from Win32_NetworkAdapterConfiguration")

For Each objItem in colItems


'optional
If objItem.Description = "WAN Miniport (SSTP)" Or objItem.Description = "WAN Miniport (IKEv2)" Or objItem.Description = "WAN Miniport (L2TP)" Or objItem.Description = "WAN Miniport (PPTP)" Or objItem.Description = "WAN Miniport (PPPOE)" Or objItem.Description = "WAN Miniport (IP)" Or objItem.Description = "WAN Miniport (IPv6)" Or objItem.Description = "WAN Miniport (Network Monitor)" Or objItem.Description = "RAS Async Adapter"  Then

'Wscript.Echo "bypass"

Else


	 'if no Network card
	If IsNull(objItem.Description) Then
		allDescript=allDescript & ",No Network card"
		
	Else
		'merge network card
		allDescript = allDescript & "," & objItem.Description
	End If
	
	
	 'if no Mac address
	If IsNull(objItem.MACAddress) Then
		allMac=allMac & ",No Mac"
	Else
		'merge mac address
		allMac = allMac & "," & objItem.MACAddress
	End If
	
	
	If Not IsNull(objItem.IPAddress) Then
	    If IsArray( objItem.IPAddress ) Then

			For i = 0 To UBound(objItem.IPAddress)
				If Not Instr(objItem.IPAddress(i), ":") > 0 Then
						'merge ips
						allIP_Addr4=allIP_Addr4 & "," & objItem.IPAddress(i)
				End If
	 
			Next
		End If

		'if no IP address
		Elseif IsNull(objItem.IPAddress) Then
			allIP_Addr4=allIP_Addr4 & ",No IP"
   End If

End if
  
  

Next

allDescript = Replace(Right(allDescript, Len(allDescript)-1),"|"," ")
allIP_Addr4 = Replace(Right(allIP_Addr4, Len(allIP_Addr4)-1),"|"," ")
allMac = Replace(Right(allMac, Len(allMac)-1),"|"," ")

	If OSARC = "" Then
					OSARC = SYSSYSTYPE
				Else
					'no error
				End If


Wscript.Echo currentDate & "|" & SYSNAME & "|" & OSCAPTION & "|" & OSVERSION & " " + OSARC & "|" + OSMANU & "|" & strOSIDATE & "|" & strOSLBOOTTIME & "|" & SYSMANU & "|" & SYSMOD & "|" & SYSSTYPE & "|" & SYSDOMAIN & "|" & allDescript & "|" & allIP_Addr4 & "|" & allMac


</script></job>

